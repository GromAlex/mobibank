//
//  AppDelegate.swift
//  Mobibank
//
//  Created by Grom on 5/7/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {



	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		// Override point for customization after application launch.
		
		
		Thread.sleep(forTimeInterval: 2.0)
		
		UITabBar.appearance().barTintColor = UIColor.cobalt
		UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: Fonts.montserratExtraBold, size: 8)!], for: .normal)
		UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: Fonts.montserratExtraBold, size: 8)!], for: .selected)
		
		UITabBar.appearance().tintColor = UIColor.brightAqua
		UITabBar.appearance().unselectedItemTintColor = .white
		
		
		let appearance = UINavigationBarAppearance()
		appearance.configureWithOpaqueBackground()
		appearance.backgroundColor = UIColor.clear

		let backButtonImage = UIImage(named: Assets.Icons.backArrow)?.withTintColor(.white)
		appearance.setBackIndicatorImage(backButtonImage, transitionMaskImage: backButtonImage)
		appearance.shadowImage = UIImage()
		appearance.shadowColor = .clear
		appearance.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont(name: Fonts.montserratSemibold, size: 12.0)!, .kern: 1.09]

		let back = UIBarButtonItemAppearance()
		// hide back button text
		back.normal.titleTextAttributes = [.foregroundColor: UIColor.clear]
		appearance.backButtonAppearance = back
		
	
		UINavigationBar.appearance().standardAppearance = appearance;
		UINavigationBar.appearance().compactAppearance = appearance;
		UINavigationBar.appearance().scrollEdgeAppearance = appearance;
		UINavigationBar.appearance().barTintColor = .white
		UINavigationBar.appearance().tintColor = .white

		
		return true
	}

	// MARK: UISceneSession Lifecycle

	func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
		// Called when a new scene session is being created.
		// Use this method to select a configuration to create the new scene with.
		return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
	}

	func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
		// Called when the user discards a scene session.
		// If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
		// Use this method to release any resources that were specific to the discarded scenes, as they will not return.
	}


}

