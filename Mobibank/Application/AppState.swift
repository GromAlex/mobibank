//
//  AppState.swift
//  Mobibank
//
//  Created by Grom on 5/12/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import Foundation

enum LaunchState {
	case firstStart
	case signIn
	case locked
	case unlocked
}

class AppState: ObservableObject{
	static let shared = AppState()
	
	@Published var showMenu: Bool = false
	@Published var showProfile: Bool = false
	@Published var launchState: LaunchState = .signIn
	@Published var tabBarSelectedIndex: Int = 0
	@Published var selectedMenuItem: MenuItem = .home{
		didSet{
			self.tabBarSelectedIndex = selectedMenuItem.tabBarIndex ?? self.tabBarSelectedIndex
		}
	}
}
