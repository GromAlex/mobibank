//
//  ApplicationRouter.swift
//  Sihaty
//
//  Created by Nikita Leshchanka on 19.03.2020.
//  Copyright © 2020 Bamboo Agile. All rights reserved.
//

import SwiftUI

struct ApplicationRouter: View {
    @EnvironmentObject var appState: AppState
    
    var body: some View {
        ZStack {
            configureView()
        }
    }
}

extension ApplicationRouter {
    private func configureView() -> some View {
		let state = appState.launchState

        switch state {
        case .unlocked:
            return AnyView(AppChrome())
		default:
            return AnyView(SignInView())
		}
    }
}
