//
//  Collection+enumeratedArray.swift
//  Zay
//
//  Created by Grom on 3/18/20.
//  Copyright © 2020 Peter Williams. All rights reserved.
//

import Foundation

extension Collection {
  func enumeratedArray() -> Array<(offset: Int, element: Self.Element)> {
    return Array(self.enumerated())
  }
}
