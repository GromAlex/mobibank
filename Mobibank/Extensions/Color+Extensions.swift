//
//  Color+Extensions.swift
//  Mobibank
//
//  Created by Grom on 5/7/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import Foundation
import SwiftUI

extension Color {
	static let cobalt = Color("cobalt")
	static let fireEngineRed = Color("fireEngineRed")
	static let brightAqua = Color("brightAqua")
	static let lightNavy  = Color("lightNavy")
	static let lightNavyTwo  = Color("lightNavyTwo")
	static let lightNavyThree  = Color("lightNavyThree")
	static let dark50 = Color("dark50")
	static let white10 = Color("white10")
	static let turquoiseBlue = Color("turquoiseBlue")
	static let appleGreen = Color("appleGreen")
	static let redPink = Color("redPink")
}
