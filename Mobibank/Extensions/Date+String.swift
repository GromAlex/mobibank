//
//  NSDate+String.swift
//  PanGaia
//
//  Created by Grom on 7/7/18.
//  Copyright © 2018 BambooApps. All rights reserved.
//

import Foundation

extension Date
{
	func toString( dateFormat format  : String ) -> String
	{
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = format
		
		return dateFormatter.string(from: self)
	}
	
}
