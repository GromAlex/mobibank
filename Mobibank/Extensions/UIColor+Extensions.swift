//
//  UIColor+Additions.swift
//  Mobibank
//
//  Generated on Zeplin. (5/7/2020).
//  Copyright (c) 2020 __MyCompanyName__. All rights reserved.
//

import UIKit

extension UIColor {
  static let cobalt = UIColor(named: "cobalt")
  static let fireEngineRed = UIColor(named: "fireEngineRed")
  static let brightAqua = UIColor(named: "brightAqua")
  static let lightNavyThree = UIColor(named: "lightNavyThree")
}
