//
//  TextKeys.swift
//  Mobibank
//
//  Created by Grom on 5/7/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import Foundation

struct TextKeys {
    
}

extension TextKeys {
	struct Copyright{
		static let title = "Copyright Mobihai Ltd — All rights Reserved"
	}
	struct SignIn {
		static let usePassword = "USE PASSWORD"
		static let createAccount = "CREATE ACCOUNT"
	}
	struct PinCode {
		static let forgotPassword = "forgot password?"
		static let wrongPassword  = "Incorrect password. Try again"
	}
	struct AppChrome {
		static let home = "HOME"
		static let creditsAndLoans = "CREDITS & LOANS"
		static let deals = "DEALS"
		static let transfers = "TRANSFERS"
		static let billings = "BILLINGS"
	}
	struct Home {
		static let accounts = "ACCOUNTS"
		static let seeAll   = "see all"
		static let more     = "more"
		static let cards = "CARDS"
		static let loans = "LOANS"
		static let credits = "CREDITS"
	}
	struct CreditsAndLoansView {
		static let title = "Credit & Loans"
		static let credits = "Credits"
		static let loans = "Loans"
		static let recentActivity = "Recent activity"
		static let pay = "Pay"
		static let apply = "Apply"
		static let addNewCredit = "+ Add new credit"
		static let addNewLoan = "Apply for a fellow loan"
		static let currentCredits = "Current Credits"
		static let currentLoans = "Current Loans"
		static let creditsOffer = "Credits offer"
		static let loansOffer = "Loans offer"
		static let viewTerms  = "View terms"
		static let decline  = "Decline"
		static let accept  = "Accept"
	}
	struct ApplyCreditView {
		static let howMuch = "How much would you like to lend?"
		static let title = "Credit application"
		static let apply = "apply"
	}
	struct DealsView {
		static let title = "Deals"
		static let allContracts = "All contracts"
		static let pendingContracts = "pending contracts"
		static let sign = "Sign"
	}
	struct InvestmentsView{
		static let title = "Investments"
		static let stockPortfolio = "Stock portfolio"
		static let stocks = "Stocks"
		static let funds = "Funds"
		static let addToPortfolio = "Add to portfolio"
		static let pay = "Pay"
		static let apply = "Apply"
		static let buy = "Buy"
		static let sell = "Sell"
	}
	struct BillingsView{
		static let title = "Billings"
		static let viewInvoiceArchive = "View invoice archive"
		static let postpone = "Postpone"
		static let payNow = "Pay now"
		static let billingPayment = "Billing payment"
		static let billingPostpone = "Billing postpone"
		static let paymentWarning = "Are you sure you want to pay %@ %@ now?"
		static let postponeWarning = "Are you sure you want to postpone %@ %@ invoice now?"
		static let applyForCredit = "Apply for credit"
		static let payOnDueDate = "Pay on due date"
		static let dueDate = "Due date %@"
		static let done = "Payment done"
		static let paymentPostponed = "Payment postponed"
		static let paymentDone = "Payment done"
	}
	struct InvestmentDetailsView{
		static let title = "%@ details"
		static let limitOrder = "Limit order"
		static let setAlerts = "Set Alerts"
	}
	struct Menu{
		static let wallets = "Wallets"
		static let trading = "Trading"
		static let criptoCredit = "Crypto credits"
		static let preferencies = "Preferences"
		static let customerSupport = "Customer support"
		static let lostCard = "Lost you card?"
		static let signOut = "Sign out"
		static let home = "Home"
		static let creditsAndLoans = "Credits & loans"
		static let deals = "Deals"
		static let transfers = "Transfers"
		static let billings = "Billings"
		static let investments = "Investments"
	}
	struct ProfileView{
		static let title = "Profile"
		static let birthDate = "Date of birth"
		static let personalId = "Personal ID"
		static let medicalId = "Medical ID"
		static let passport = "Passport"
		static let driversLicense = "Drivers License"
		static let addMoreDocument = "Add more document"
	}
	struct Transfers{
		static let title = "Money Transfers"
		static let howMuch = "How much would you like to transfer?"
		static let amountPlaceholder = "Type the ammount"
		static let transfer = "transfer"
		static let dueDate = "Due date"
		static let whichAccount = "From which account?"
		static let chooseRecepient = "Choose recipient"
		static let contacts = "Contacts"
		static let done = "Transfer done"
		static let template = "Create a template"
		static let viewPayslip = "view payslip"
	}
	struct CreditPaymentView{
		static let title = "Credit payment"
		static let howMuch = "How much would you like to pay?"
		static let pay = "Pay"
	}
	struct CreditApplyView{
		static let title = "Credit application"
		static let howMuch = "How much would you like to lend?"
		static let apply = "Apply"
		static let applicationSubmitted = "Application Submitted"
	}
}
