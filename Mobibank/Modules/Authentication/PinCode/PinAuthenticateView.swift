//
//  EnterPinView.swift
//  Sihaty
//
//  Created by Nikita Leshchanka on 01.04.2020.
//  Copyright © 2020 Bamboo Agile. All rights reserved.
//

import SwiftUI

struct PinKeyboardButtonStyle: ButtonStyle {
   
    public func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
			.foregroundColor(configuration.isPressed ? Color.cobalt : Color.white)
			.background(background(configuration.isPressed))
    }
	
	func background(_ isPressed: Bool)-> some View{
		Group{
			if isPressed{
				Circle().fill(Color.white)
			}
			else{
				Circle().stroke(Color.white)
			}
		}
		.frame(width: 70, height: 70)
	}
}


struct PinAuthenticateView: View {
    @ObservedObject private var viewModel = PinAuthenticateViewModel()
    @EnvironmentObject private var appState: AppState
        
    private var screenHeight: CGFloat {
        return UIScreen.main.bounds.height
    }
    
	var body: some View {
		ZStack {
			Background(withPoints: true, withLogo: false)
			
			VStack(alignment: .center, spacing: 0) {
//				Image(Assets.Icons.logo)
//					.scaleEffect(0.75)
//					.padding(.top, 65)
				HeaderView(type: .pushed)
				
				PinIndicatorView(numberOfFilledDots: viewModel.pincode.count)
					.frame(width: UIScreen.main.bounds.size.width)
					.padding(.top, screenHeight * 0.032)
					.animation(nil)
					.modifier(ShakeEffect(shakes: viewModel.invalidAttempts * 2))
					.animation(Animation.linear)

				
				Text(TextKeys.PinCode.wrongPassword)
					.font(.custom(Fonts.montserratSemibold, size: 12))
					.foregroundColor(Color.fireEngineRed)
					.padding(.top, 20)
					.opacity(viewModel.pincodeIncorrect ? 1 : 0)
				
				
				pinKeyboard
					.padding(.top, screenHeight * 0.082)
				
				Spacer()
				
				Button(action: {
					self.viewModel.isForgotPasswordPresented.toggle()
				}) {
					Text(TextKeys.PinCode.forgotPassword.uppercased())
						.foregroundColor(.white)
						.font(.custom(Fonts.montserratSemibold, size: 10))
						.padding(.top, screenHeight * 0.05847)
				}
				.padding(.bottom, 23)
				
				//                    NavigationLink(destination: ForgotPasswordView(), isActive: $viewModel.isForgotPasswordPresented) {
				//                        EmptyView()
				//                    }
			}
		}
		.edgesIgnoringSafeArea(.top)
		.hiddenNavigationBarStyle()
		.onReceive(viewModel.$isLoginFinished) { isLoginFinished in
			guard isLoginFinished else { return }
		    self.appState.launchState = .unlocked
		}
	}
}

extension PinAuthenticateView {
	private var screenSize: CGSize {
		return UIScreen.main.bounds.size
	}
	
	private var verticalSpacing: CGFloat {
		return screenHeight * 0.07847
	}
	
	private var horizontalSpacing: CGFloat {
		return 30
	}
	
	private var buttonHeight: CGFloat {
		return screenHeight * 0.0531176
	}
	
	var pinKeyboard: some View {
		VStack(alignment: .center, spacing: verticalSpacing) {
			HStack(spacing: horizontalSpacing) {
				PinKeyboardButton(number: 1, pin: $viewModel.pincode)
				PinKeyboardButton(number: 2, pin: $viewModel.pincode)
				PinKeyboardButton(number: 3, pin: $viewModel.pincode)
			}
			
			HStack(spacing: horizontalSpacing) {
				PinKeyboardButton(number: 4, pin: $viewModel.pincode)
				PinKeyboardButton(number: 5, pin: $viewModel.pincode)
				PinKeyboardButton(number: 6, pin: $viewModel.pincode)
			}
			
			HStack(spacing: horizontalSpacing) {
				PinKeyboardButton(number: 7, pin: $viewModel.pincode)
				PinKeyboardButton(number: 8, pin: $viewModel.pincode)
				PinKeyboardButton(number: 9, pin: $viewModel.pincode)
			}
			
			HStack(spacing: horizontalSpacing) {
				Color.clear
					.frame(width: screenSize.width * 0.19, height: buttonHeight)
				PinKeyboardButton(number: 0, pin: $viewModel.pincode)
				PinKeyboardEraseButton(pin: $viewModel.pincode)
			}
		}
	}
}

private struct PinKeyboardButton: View {
	private let screenSize = UIScreen.main.bounds.size
	
	private var buttonHeight: CGFloat {
		return screenSize.height * 0.0531176
	}
	
	private var size: CGSize {
		return CGSize(width: screenSize.width * 0.19, height: buttonHeight)
	}
	
	let number: Int
	@Binding var pin: String
	
	var body: some View {
		Button(action: {
			guard self.pin.count < 6 else { return }
			self.pin.append("\(self.number)")
		}) {
			Text("\(number)")
				.font(.custom(Fonts.montserratSemibold, size: 30))
				.frame(width: size.width, height: size.height)
		}
		.buttonStyle(PinKeyboardButtonStyle())
	}
}

private struct PinKeyboardEraseButton: View {
	private let screenSize = UIScreen.main.bounds.size
	
	private var buttonHeight: CGFloat {
		return screenSize.height * 0.0531176
	}
	
	private var size: CGSize {
		return CGSize(width: screenSize.width * 0.19, height: buttonHeight)
	}
	
	@Binding var pin: String
	
	var body: some View {
		Button(action: {
			guard self.pin.count > 0 else { return }
			self.pin.removeLast()
		}) {
			ZStack {
				RoundedRectangle(cornerRadius: size.width / 2)
                    .foregroundColor(.clear)
                
                Image(Assets.Icons.delete)
                    .resizable()
                    .frame(width: 34, height: 25)
                    .foregroundColor(.white)
            }.frame(width: size.width, height: size.height)
        }
    }
}

struct EnterPinView_Previews: PreviewProvider {
    static var previews: some View {
        PinAuthenticateView()
    }
}
