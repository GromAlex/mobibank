//
//  PinAuthenticateViewModel.swift
//  Sihaty
//
//  Created by Nikita Leshchanka on 03.04.2020.
//  Copyright © 2020 Bamboo Agile. All rights reserved.
//

import SwiftUI

final class PinAuthenticateViewModel: ObservableObject {
    @Published var pincode = "" {
        didSet {
            print(pincode)
            pincodeDidChange()
        }
    }
    @Published var isForgotPasswordPresented = false
    @Published var isLoginFinished = false
    @Published var isPincodeCorrect: Bool?
    
	@Published var invalidAttempts: Int = 0
	
    private func pincodeDidChange() {
        if isPincodeCorrect == false, !pincode.isEmpty {
            isPincodeCorrect = nil
        }
        
        guard pincode.count == 6 else { return }
        
        let securePin = "123456"//KeychainManager.get(key: .securePin) ?? ""
        
        if securePin != "", pincode == securePin {
            isPincodeCorrect = true
           // UserDefaultsConfig.isLocked = false
            isLoginFinished = true
        } else {
            isPincodeCorrect = false
            pincode = ""
			invalidAttempts += 1
        }
    }
	
	var pincodeIncorrect: Bool{
		isPincodeCorrect != nil && isPincodeCorrect == false
	}
}
