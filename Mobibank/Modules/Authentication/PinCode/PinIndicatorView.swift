//
//  PinIndicatorView.swift
//  Sihaty
//
//  Created by Nikita Leshchanka on 01.04.2020.
//  Copyright © 2020 Bamboo Agile. All rights reserved.
//

import SwiftUI

struct PinIndicatorView: View {
	private var emptyDotImage: some View{
		Circle().stroke(Color.white)
	}
    private let dotImage = Image(systemName: "circle.fill").resizable()
    
    private let normalColor: Color = .clear
    private let selectedColor: Color = .white
    
    private let size = CGRect(x: 0, y: 0, width: 10, height: 10)
    private let numberOfDots: Int = 6
    
    let numberOfFilledDots: Int
    
    var body: some View {
        HStack(spacing: 11) {
            ForEach(0..<numberOfDots, id: \.self) { index in
                Group {
                    if index >= self.numberOfFilledDots {
                        self.emptyDotImage
                            .frame(width: self.size.width, height: self.size.height)
                            .foregroundColor(self.normalColor)
                    } else {
                        self.dotImage
                            .frame(width: self.size.width, height: self.size.height)
                            .foregroundColor(self.selectedColor)
                    }
                }
            }
        }
        .flipsForRightToLeftLayoutDirection(false)
    }
}
