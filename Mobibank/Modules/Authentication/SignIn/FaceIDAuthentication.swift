//
//  FaceIDAuthentication.swift
//  PanGaia
//
//  Created by Grom on 9/17/18.
//  Copyright © 2018 BambooApps. All rights reserved.
//

import Foundation
import LocalAuthentication

class BiometricAuhentication{
	
	enum Result {
        case success
        case scanFailure(Error?)
        case bioUnavailable(Error?)
    }
    
	enum AuthenticationCase {
        case login
        case signUpSetup
        
        var localizedReason: String {
            switch self {
            case .login:
                return ""
            case .signUpSetup:
                return ""
            }
        }
        
        var canEvaluatePolicy: LAPolicy {
            switch self {
            case .login: return .deviceOwnerAuthentication
            case .signUpSetup: return .deviceOwnerAuthenticationWithBiometrics
            }
        }
        
        var evaluatePolicy: LAPolicy {
            switch self {
            case .login: return .deviceOwnerAuthentication
            case .signUpSetup: return .deviceOwnerAuthenticationWithBiometrics
            }
        }
    }
	
	static func run(for authCase: AuthenticationCase, completion: @escaping (Result) -> Void){
		let context = LAContext()
		context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
		
		var error: NSError?
        
		let reason = NSLocalizedString("Log in to your account", comment: "")
		
        if context.canEvaluatePolicy(authCase.canEvaluatePolicy, error: &error) {
            
            context.evaluatePolicy(authCase.evaluatePolicy, localizedReason: reason) { success, error in
                guard success else {
                    completion(.scanFailure(error))
                    return
                }
                
                DispatchQueue.main.async {
                    completion(.success)
                }
            }
        }
		else{
			DispatchQueue.main.async {
				 completion(.bioUnavailable(error))
			}
		}
	}
}
