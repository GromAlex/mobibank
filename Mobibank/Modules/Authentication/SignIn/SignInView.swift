//
//  SignInView.swift
//  Mobibank
//
//  Created by Grom on 5/7/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import SwiftUI

struct SignInView: View {
	@ObservedObject var viewModel = SignInViewModel()
	@EnvironmentObject var appState: AppState
	
	var body: some View {
		NavigationView{
			ZStack{
				Background(withPoints: true, withLogo: true)
				
				VStack(spacing: 50){
					Spacer()
					Image(viewModel.biometricStatusIcon)
					HStack{
						Button(action:{
							self.viewModel.showPinCode = true
						}){
							Text(TextKeys.SignIn.usePassword)
								.font(.custom(Fonts.montserratSemibold, size: 10))
								.foregroundColor(.white)
						}
						Spacer()
						Button(action:{}){
							Text(TextKeys.SignIn.createAccount)
								.font(.custom(Fonts.montserratSemibold, size: 10))
								.foregroundColor(.white)
						}
					}
					.padding(.horizontal, 30)
					.padding(.bottom, 23)
				}
				
				NavigationLink(destination: PinAuthenticateView(), isActive: $viewModel.showPinCode){
					EmptyView()
				}
			}
			.edgesIgnoringSafeArea(.top)
			.onAppear(){
				self.viewModel.faceIdAuth()
			}
			.onReceive(viewModel.$status.filter{$0 == .success}) {_ in
				self.appState.launchState = .unlocked
			}
		}
	}
}


struct SignInView_Previews: PreviewProvider {
    static var previews: some View {
        SignInView()
    }
}
