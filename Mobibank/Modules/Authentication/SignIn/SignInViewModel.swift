//
//  SignInViewModel.swift
//  Mobibank
//
//  Created by Grom on 5/7/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import Foundation

enum SignInStatus{
	case undefined
	case success
	case failure
}

class SignInViewModel: ObservableObject{
	@Published var showPinCode: Bool = false
	@Published var status: SignInStatus = .undefined
	
	func faceIdAuth(){
		BiometricAuhentication.run(for: .signUpSetup) {result in
			switch result {
            case .success:
				DispatchQueue.main.async {
					self.status = .success
				}
            case .scanFailure(_), .bioUnavailable(_):
				DispatchQueue.main.async {
					self.status = .failure
				}
            }
		}
	}
	
	var biometricStatusIcon: String{
		switch status{
		case .undefined:
			return Assets.Icons.faceId
		case .success:
			return Assets.Icons.correct
		case .failure:
			return Assets.Icons.wrong
		}
	}
}
