//
//  BillingPaymentView.swift
//  Mobibank
//
//  Created by Grom on 5/13/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import SwiftUI

struct BillingPaymentView: View {
	
	let billing: Billing
	@State var showDone: Bool = false
	
    var body: some View {
        ZStack{
			Background(color: .lightNavy)
			
			VStack(spacing: 0){
				HeaderView(title: TextKeys.BillingsView.billingPayment, type: .pushed)
				
				ScrollView{
					VStack(alignment: .leading, spacing: 30){
						warningText
						invoiceText
					}
					.font(.custom(Fonts.montserratSemibold, size: 24))
					.foregroundColor(.white)
					.padding(.horizontal, 25)
					.padding(.top, 25)
					
					Spacer()
					
					buttons
						.padding(.bottom, 115)
						.padding(.top, 50)
				}
			}
		}
		.edgesIgnoringSafeArea(.top)
		.hiddenNavigationBarStyle()
		.sheet(isPresented: $showDone){
			PaymentDoneView(title: TextKeys.BillingsView.billingPayment, description: TextKeys.BillingsView.paymentDone, isViewPayslip: true, isTemplate: true)
		}
    }
	
	var warningText: some View{
		
		Text("Are you sure you want to pay ") +
			Text("\(billing.title)") +
			Text(" \(billing.value) ").foregroundColor(.brightAqua) +
			Text("now?")
	}
	
	var invoiceText: some View{
		
		Text("The invoice is due on the") +
			Text(" \(billing.date) ").foregroundColor(.brightAqua)
	}
	
	var buttons: some View{
		VStack(spacing: 17){
			BigStrokedButton(title: TextKeys.BillingsView.applyForCredit)
			BigStrokedButton(title: TextKeys.BillingsView.payOnDueDate)
			Button(action: {
				self.showDone.toggle()
			}){
				BigRoundedButton(title: TextKeys.BillingsView.payNow)
			}
		}
		
	}
}

struct BillingPaymentView_Previews: PreviewProvider {
    static var previews: some View {
		BillingPaymentView(billing: Billing(title: "Telia", date: "Due date 03.12.2019", value: "€ 29,90"))
    }
}
