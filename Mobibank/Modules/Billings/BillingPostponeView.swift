//
//  BillingPostponeView.swift
//  Mobibank
//
//  Created by Grom on 5/13/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import SwiftUI

struct BillingPostponeView: View {
	let billing: Billing
	
	let titles = ["Free postpone 7 days", "+ € 5,00 postpone 14 days"]
	
	@State var selection: Int = 0
	@State var showDone: Bool = false
	
	var body: some View {
		ZStack{
			Background(color: .lightNavy)
			
			VStack(alignment: .leading, spacing: 0){
				HeaderView(title: TextKeys.BillingsView.billingPostpone, type: .pushed)
				
				ScrollView{
					VStack(alignment: .leading, spacing: 30){
						warningText//.fixedSize(horizontal: false, vertical: true)
						invoiceText//.fixedSize(horizontal: false, vertical: true)
					}
					.font(.custom(Fonts.montserratSemibold, size: 24))
					.foregroundColor(.white)
					.padding(.horizontal, 25)
					.padding(.top, 25)
					
					Spacer()
					
					RadioGroup(selectedIndex: $selection, titles: titles)
						.padding(.leading, 25)
						.padding(.bottom, 76)
						.padding(.top, 40)
					
					buttons.padding(.bottom, 115)
				}
			}
		}
		.edgesIgnoringSafeArea(.top)
		.hiddenNavigationBarStyle()
		.sheet(isPresented: $showDone){
			PaymentDoneView(title: TextKeys.BillingsView.billingPostpone, description: TextKeys.BillingsView.paymentPostponed)
		}
	}
	
	var warningText: some View{
		
		Text("Are you sure you want to postpone ") +
			Text("\(billing.title)'s") +
			Text(" \(billing.value) ").foregroundColor(.brightAqua) +
			Text("invoice now?")
	}
	
	var invoiceText: some View{
		
		Text("The invoice is due on the") +
			Text(" \(billing.date) ").foregroundColor(.brightAqua)
	}
	
	var buttons: some View{
		HStack{
			Spacer()
			Button(action: {
				self.showDone.toggle()
			}){
				BigRoundedButton(title: TextKeys.BillingsView.postpone)
			}
			Spacer()
		}
	}
}

struct BillingPostponeView_Previews: PreviewProvider {
	static var previews: some View {
		BillingPostponeView(billing: Billing(title: "Telia", date: "Due date 03.12.2019", value: "€ 29,90"))
	}
}
