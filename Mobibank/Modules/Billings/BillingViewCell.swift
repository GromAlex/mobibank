//
//  BillingViewCell.swift
//  Mobibank
//
//  Created by Grom on 5/13/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import SwiftUI

struct BillingViewCell: View {
	
	let billing: Billing
	
    var body: some View {
        VStack(alignment: .leading, spacing: 0){
			HStack{
				Text(billing.title.uppercased())
					.font(.custom(Fonts.montserratSemibold, size: 12))
					.foregroundColor(.white)
					.kerning(1.09)
				Spacer()
				
				Text(billing.value)
					.font(.custom(Fonts.montserratSemibold, size: 15))
					.kerning(0.13)
					.foregroundColor(.white)
				
			}
			.padding(.top, 13)
			
			Text(String(format:TextKeys.BillingsView.dueDate, billing.date))
				.font(.custom(Fonts.montserratRegular, size: 12))
				.kerning(1.09)
				.foregroundColor(.white)
				.padding(.bottom, 10)
			
			buttons.padding(.bottom, 8)
			
			Divider().frame(height: 1).background(Color.white10).padding(.top, 10)
		}
    }
	
	var buttons: some View{
		HStack{
			NavigationLink(destination: BillingPostponeView(billing: billing)){
				RoundedButton(title: TextKeys.BillingsView.postpone, icon: Assets.Icons.arrowNext)
			}
			.buttonStyle(PlainButtonStyle())
			
			NavigationLink(destination: BillingPaymentView(billing: billing)){
				RoundedButton(title: TextKeys.BillingsView.payNow, icon: Assets.Icons.arrowUp)
			}
			.buttonStyle(PlainButtonStyle())
			
			Spacer()
		}
	}
}

struct BillingViewCell_Previews: PreviewProvider {
    static var previews: some View {
        BillingViewCell(billing: Billing(title: "Telia", date: "Due date 03.12.2019", value: "€ 29,90"))
    }
}
