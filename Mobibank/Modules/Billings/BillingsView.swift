//
//  BillingsView.swift
//  Mobibank
//
//  Created by Grom on 5/12/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import SwiftUI

struct BillingsView: View {
	
	@ObservedObject var viewModel = BillingsViewModel()
	var body: some View {
		NavigationView{
			ZStack(alignment: .top){
				Background(color:.lightNavy)
				
				VStack(spacing: 0){
					
					header
					
					ScrollView{
						VStack(spacing: 0){
							ForEach(viewModel.billings, id: \.id){
								BillingViewCell(billing: $0)
							}
						}
						.padding(.horizontal, 25)
						.padding(.top, 15)
					}
					
					Spacer()
					
					BigRoundedButton(title: TextKeys.BillingsView.viewInvoiceArchive).padding(.vertical, 40)
					
					Spacer()
				}
			}
			.edgesIgnoringSafeArea(.top)
			.hiddenNavigationBarStyle()
		}
	}
	
	var header: some View{
		Group{
			HeaderView(title: TextKeys.BillingsView.title)
		}
		.background(Color.lightNavyThree)
	}
}

struct BillingsView_Previews: PreviewProvider {
    static var previews: some View {
        BillingsView()
    }
}
