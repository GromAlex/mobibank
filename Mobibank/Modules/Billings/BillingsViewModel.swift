//
//  BillingsViewModel.swift
//  Mobibank
//
//  Created by Grom on 5/12/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import Foundation

class BillingsViewModel: ObservableObject{
	@Published var billings = [Billing(title: "Telia", date: "03.12.2019", value: "€ 29,90"),
							   Billing(title: "Tampereen sahkolaitos", date: "05.12.2019", value: "€ 158,56")]
}
