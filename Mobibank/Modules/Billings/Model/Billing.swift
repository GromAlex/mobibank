//
//  Billing.swift
//  Mobibank
//
//  Created by Grom on 5/13/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import Foundation


struct Billing: Identifiable{
	let title: String
	let date: String
	let value: String
	var id = UUID()
}
