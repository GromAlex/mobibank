//
//  ApplyCreditView.swift
//  Mobibank
//
//  Created by Grom on 5/11/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import SwiftUI

struct ApplyCreditView: View {
	
	let credit: CreditItem
	
    var body: some View {
		ZStack{
			Background(color: .lightNavy)
			
			VStack{
				Text(TextKeys.ApplyCreditView.howMuch)
					.font(.custom(Fonts.montserratMedium, size: 24))
					.kerning(0.21)
					.foregroundColor(.white)
					.padding(.vertical, 30)
				
				CreditViewCell(credit: credit, valueAlignment: .top)
					.padding(.vertical, 32)
				
				Spacer()
				
				BigRoundedButton(title: TextKeys.ApplyCreditView.apply).padding(.vertical, 33)
			}
			.padding(.horizontal, 25)
		}
		.navigationBarTitle(LocalizedStringKey(TextKeys.ApplyCreditView.title.uppercased()), displayMode: .inline)
	}
}

struct ApplyCreditView_Previews: PreviewProvider {
    static var previews: some View {
        ApplyCreditView(credit: Credit(name: "fsf", description: "sdf", value: "sdf"))
    }
}
