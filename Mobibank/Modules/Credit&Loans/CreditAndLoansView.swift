//
//  CreditAndLoansView.swift
//  Mobibank
//
//  Created by Grom on 5/11/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import SwiftUI

struct CreditAndLoansView: View {
	
	@ObservedObject var viewModel = CreditAndLoansViewModel()
	
	var body: some View {
		NavigationView{
			ZStack(alignment: .top){
				Background(color:.lightNavy)
				
				VStack(spacing: 0){
					
					header
					
					ScrollView{
						VStack(spacing: 0){
							content
						}
						.padding(.horizontal, 25)
					}
					
					Spacer()
					
					BigRoundedButton(title: viewModel.addButtonTitle).padding(.vertical, 40)
					
				}
			}
			.edgesIgnoringSafeArea(.top)
			.hiddenNavigationBarStyle()
		}
	}
	
	var header: some View{
		Group{
			HeaderView(title: TextKeys.CreditsAndLoansView.title)
			CustomTabView(tabs: viewModel.tabs, selectedIndex: $viewModel.selectedTabIndex)
				.padding(.horizontal, 45)
				.padding(.top, 18)
		}
		.background(Color.lightNavyThree)
	}
	
	var content: some View{
		Group{
			if viewModel.selectedTabIndex == 0{
				
				BlockTitle(title: viewModel.currentTitle)
				.padding(.top, 20)

				ForEach(viewModel.selectedCredits, id: \.id){ credit in
					CurrentCreditViewCell(credit: credit)
				}

				BlockTitle(title: viewModel.offerTitle)
					.padding(.top, 20)
				
				ForEach(viewModel.selectedOfferedCredits, id: \.id){ credit in
					OfferCreditViewCell(credit: credit)
				}
			}
			else{
				ForEach(viewModel.loans, id: \.id){ loan in
					LoanViewCell(loan: loan)
				}
				.padding(.top, 10)
			}
		}
	}
}


struct BlockTitle: View{
	let title: String
	var body: some View{
		HStack{
			Text(title.uppercased())
				.font(.custom(Fonts.montserratRegular, size: 15))
				.kerning(1.36)
				.foregroundColor(.white)
			Spacer()
		}
	}
}


struct CreditAndLoansView_Previews: PreviewProvider {
    static var previews: some View {
        CreditAndLoansView()
    }
}

