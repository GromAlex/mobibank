//
//  CreditAndLoansViewModel.swift
//  Mobibank
//
//  Created by Grom on 5/11/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import Foundation

class CreditAndLoansViewModel: ObservableObject{
	
	let tabs = [TextKeys.CreditsAndLoansView.credits, TextKeys.CreditsAndLoansView.loans]
	
	@Published var selectedTabIndex: Int = 0
	
	@Published var loans: [Loan] = [Loan(name: "Sakari Pietitla", description: "€ 200 due in 4 days", value: "€ 2.000,00", interest: "Interest 12.40%"),
										  Loan(name: "Hannu Turpeinen", description: "€ 500.00 due in 13 days", value: "€ 2.000,00", interest: "Interest 7.8%")]
							
	
	@Published var credits: [CreditItem] = [Credit(name: "Fellow credit", description: "Interest 12.00%", value: "€ 442,40"),
							  Credit(name: "Nordea Joustoluotto", description: "Interest 9.50%", value: "€ 10.000,00"),
							  Credit(name: "Nordea Mortgage", description: "Interest 2.5%", value: "€ 270.000,00")]
	
	var offeredCredits:  [CreditItem]  = [Credit(name: "OP Mastercard Offer", description: "Interest 18.99%", value: "€ 2.000,00")]
	var offeredLoans:  [CreditItem]  = [Credit(name: "OP Mastercard Offer", description: "Interest 20.99%", value: "€ 5.000,00")]
	
	var selectedCredits: [CreditItem]{
		selectedTabIndex == 0 ? credits : loans
	}
	
	var selectedOfferedCredits:[CreditItem]{
		selectedTabIndex == 0 ? offeredCredits : offeredLoans
	}
	
	var currentTitle: String{
		selectedTabIndex == 0 ? TextKeys.CreditsAndLoansView.currentCredits : TextKeys.CreditsAndLoansView.currentLoans
	}
	
	var offerTitle: String{
		selectedTabIndex == 0 ? TextKeys.CreditsAndLoansView.creditsOffer : TextKeys.CreditsAndLoansView.loansOffer
	}
	
	var addButtonTitle: String{
		selectedTabIndex == 0 ? TextKeys.CreditsAndLoansView.addNewCredit : TextKeys.CreditsAndLoansView.addNewLoan
	}
}
