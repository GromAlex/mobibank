//
//  CreditApplyVIew.swift
//  Mobibank
//
//  Created by Grom on 5/14/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import SwiftUI

struct CreditApplyView: View {
	
	@ObservedObject var viewModel = CreditApplyViewModel()
	let credit: CreditItem
	
    var body: some View {
        ZStack{
			Background(color: .lightNavy)
			
			VStack(spacing: 0){
				HeaderView(title: TextKeys.CreditApplyView.title, type: .pushed)
				
				VStack(spacing: 0){
					
					howMuchText
					
					CreditViewCell(credit: credit, valueAlignment: .top)
						.padding(.top, 28)
					    .padding(.bottom, 20)
					
					LabelTextField(placeHolder: TextKeys.Transfers.amountPlaceholder, text: $viewModel.moneyValue, keyboardType: .numberPad)
						.padding(.top, 20)
						.padding(.bottom, 23)
					
					ChooseDateView(showDatePicker: $viewModel.showDatePicker, date: viewModel.dateFormatted)
						.padding(.bottom, 20)
					
					Button(action:{
						self.viewModel.showDone.toggle()
					}){
						BigRoundedButton(title: TextKeys.CreditApplyView.apply)
							.opacity(viewModel.buttonDisabled ? 0.4 : 1)
					}
					.disabled(viewModel.buttonDisabled)
					Spacer()
					
				}
				.padding(.horizontal, 25)
				
			}
		}
		.edgesIgnoringSafeArea(.top)
		.hiddenNavigationBarStyle()
		.customBottomSheet(isPresented: $viewModel.showDatePicker){
			DatePickerView(date: self.$viewModel.date)
		}
		.sheet(isPresented: $viewModel.showDone){
			PaymentDoneView(title:  TextKeys.CreditApplyView.title, description: TextKeys.CreditApplyView.applicationSubmitted)
		}
    }
	
	var howMuchText: some View{
		Text(TextKeys.CreditApplyView.howMuch)
			.font(.custom(Fonts.montserratSemibold, size: 24))
			.foregroundColor(.white)
			.padding(.top, 25)
	}
}
