//
//  CreditApplyViewModel.swift
//  Mobibank
//
//  Created by Grom on 5/14/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import Foundation

class CreditApplyViewModel: ObservableObject{
	@Published var moneyValue: String = ""
	@Published var showDatePicker: Bool = false
	@Published var showAccountPicker: Bool = false
	@Published var date = Date()
	
	@Published var accounts: [Account] = [Account(name: "Deposit account", value: "€ 10.456,77", number: "fi 32 4220 4222 0440 32"),
											Account(name: "Salary account", value: "€ 5.276,00", number: "fi 54 4553 3533 2323 34")]
	@Published var accountSelection: Int = 0
	@Published var showDone: Bool = false
	
	var selectedAccount: Account{
		accounts[accountSelection]
	}
	
	var dateFormatted: String{
		date.toString(dateFormat: "MMMM d, yyyy")
	}
	
	var buttonDisabled: Bool{
		moneyValue.isEmpty
	}
}
