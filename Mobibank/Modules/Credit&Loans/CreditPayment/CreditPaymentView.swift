//
//  CreditPaymentView.swift
//  Mobibank
//
//  Created by Grom on 5/14/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import SwiftUI

struct CreditPaymentView: View {
	
	let credit: CreditItem
	
	@ObservedObject var viewModel = CreditPaymentViewModel()
	
	var body: some View {
		ZStack{
			Background(color: .lightNavy)
			
			VStack(spacing: 0){
				HeaderView(title: TextKeys.CreditPaymentView.title, type: .pushed)
				
				ScrollView{
					VStack(spacing: 0){
						
						howMuchText.fixedSize(horizontal: false, vertical: true)
						
						CreditViewCell(credit: credit, valueAlignment: .top)
							.padding(.top, 28)
							.padding(.bottom, 20)
						
						LabelTextField(placeHolder: TextKeys.Transfers.amountPlaceholder, text: $viewModel.moneyValue, keyboardType: .numberPad)
							.padding(.top, 20)
							.padding(.bottom, 23)
						
						ChooseAccountView(showAccountPicker: $viewModel.showAccountPicker, account: viewModel.selectedAccount)
							.padding(.bottom, 23)
						
						ChooseDateView(showDatePicker: $viewModel.showDatePicker, date: viewModel.dateFormatted)
							.padding(.bottom, 20)
						
						Button(action: {
							self.viewModel.showDone.toggle()
						}){
							BigRoundedButton(title: TextKeys.CreditPaymentView.pay)
							.opacity(viewModel.buttonDisabled ? 0.4 : 1)
							.padding(.bottom, 20)
						}
						.disabled(viewModel.buttonDisabled)
						Spacer()
						
					}
					.padding(.horizontal, 25)
				}
				
			}
		}
		.edgesIgnoringSafeArea(.top)
		.hiddenNavigationBarStyle()
		.customBottomSheet(isPresented: $viewModel.showDatePicker){
			DatePickerView(date: self.$viewModel.date)
		}
		.customBottomSheet(isPresented: $viewModel.showAccountPicker){
			AccountPicker(accounts: self.$viewModel.accounts, selection: self.$viewModel.accountSelection)
		}
		.sheet(isPresented: $viewModel.showDone){
			PaymentDoneView(title:  TextKeys.CreditPaymentView.title, description: TextKeys.BillingsView.paymentDone, isViewPayslip: true)
		}
		
	}
	
	var howMuchText: some View{
		Text(TextKeys.CreditPaymentView.howMuch)
			.font(.custom(Fonts.montserratSemibold, size: 24))
			.foregroundColor(.white)
			.padding(.top, 25)
	}
}

struct CreditPaymentView_Previews: PreviewProvider {
	static var previews: some View {
		CreditPaymentView(credit: Credit(name: "Crypto credit", description: "r3kmLJN5D28dHuH8v…", value: "€ 2.000,00"))
	}
}
