//
//  CreditViewCellExtended.swift
//  Mobibank
//
//  Created by Grom on 5/11/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import SwiftUI

struct CurrentCreditViewCell: View {
	let credit: CreditItem
    var body: some View {
		VStack(spacing: 0){
			CreditViewCell(credit: credit, valueAlignment: .top).padding(.bottom, 10)
			HStack{
				RoundedButton(title: TextKeys.CreditsAndLoansView.recentActivity)
				Spacer()
				NavigationLink(destination: CreditPaymentView(credit: credit)){
					RoundedButton(title:  TextKeys.CreditsAndLoansView.pay, icon: Assets.Icons.arrowUp)
				}
				.buttonStyle(PlainButtonStyle())
				NavigationLink(destination: CreditApplyView(credit: credit)){
					RoundedButton(title:  TextKeys.CreditsAndLoansView.apply, icon: Assets.Icons.arrowDown)
				}
				.buttonStyle(PlainButtonStyle())
			}
			Divider().frame(height: 1).background(Color.white10).padding(.top, 8)
		}
		.padding(.top, 13)
    }
}



struct OfferCreditViewCell: View {
	let credit: CreditItem
    var body: some View {
		VStack(spacing: 0){
			CreditViewCell(credit: credit).padding(.bottom, 10)
			HStack{
				RoundedButton(title: TextKeys.CreditsAndLoansView.viewTerms)
				Spacer()
				RoundedButton(title:  TextKeys.CreditsAndLoansView.decline)
				RoundedButton(title:  TextKeys.CreditsAndLoansView.accept)
			}
			Divider().frame(height: 1).background(Color.white10).padding(.top, 8)
		}
		.padding(.top, 13)
    }
}


struct LoanViewCell: View{
	let loan: Loan
	var body: some View{
		VStack(spacing: 0){
			HStack{
				VStack(alignment:.leading, spacing: 0){
					Text(loan.name.uppercased())
						.font(.custom(Fonts.montserratSemibold, size: 12))
						.foregroundColor(.white)
						.kerning(1.09)
					
				
					Text(loan.interest)
							.font(.custom(Fonts.montserratRegular, size: 12))
							.kerning(0.13)
							.foregroundColor(.white)
					
					Text(loan.description)
						.font(.custom(Fonts.montserratRegular, size: 12))
						.kerning(0.13)
						.foregroundColor(.white)
						.padding(.top, 12)
				}
				
				Spacer()
				
				Text(loan.value)
					.font(.custom(Fonts.montserratSemibold, size: 15))
					.kerning(0.13)
					.foregroundColor(.white)
				Image(Assets.Icons.moreArrow)
				
			}
			Divider().frame(height: 1).background(Color.white10).padding(.top, 16)
		}
		.padding(.top, 14)
	}
}

struct CreditViewCellExtended_Previews: PreviewProvider {
    static var previews: some View {
        CurrentCreditViewCell(credit: Credit(name: "fsf", description: "sdf", value: "sdf"))
    }
}



struct RoundedButton: View {
	
	let title: String
	var icon: String?
	
	var body: some View {
		HStack(spacing: 0){
			Text(title)
				.font(.custom(Fonts.montserratRegular, size: 12))
				.foregroundColor(.white)
				.kerning(1.09)
				.frame(height: 30)
				.fixedSize()
			
			Unwrap(icon){icon in
				Image(icon).padding(.leading, 10)
			}
		}
		.padding(.horizontal, 15)
		.background(Color.dark50)
		.cornerRadius(15)
	}
}
