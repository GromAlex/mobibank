//
//  ContactViewCell.swift
//  Mobibank
//
//  Created by Grom on 5/12/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import SwiftUI

struct ContactViewCell: View {
	let contract: Contract
    var body: some View {
		VStack(alignment: .leading, spacing: 0){
			HStack{
				VStack(alignment: .leading, spacing: 0){
					Text(contract.name.uppercased())
						.font(.custom(Fonts.montserratSemibold, size: 12))
						.foregroundColor(.white)
						.kerning(1.09)
					Text(contract.type)
						.font(.custom(Fonts.montserratRegular, size: 12))
						.foregroundColor(.white)
						.kerning(1.09)
				}
				Spacer()
				
				Image(Assets.Icons.moreArrow)
			}
			.padding(.top, 14)
			
			Unwrap(contract.date){ date in
				HStack(alignment: .center){
					Text(date)
						.font(.custom(Fonts.montserratRegular, size: 12))
						.foregroundColor(.white)
						.kerning(1.09)
					Spacer()
					RoundedButton(title: TextKeys.DealsView.sign)
				}
				.padding(.top, 16)
			}
			Divider().frame(height: 1).background(Color.white10).padding(.top, 15)
		}
    }
}

struct ContactViewCell_Previews: PreviewProvider {
    static var previews: some View {
        ContactViewCell(contract: Contract(name: "sdf", type: "sdf"))
    }
}
