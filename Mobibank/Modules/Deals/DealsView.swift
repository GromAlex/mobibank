//
//  DealsView.swift
//  Mobibank
//
//  Created by Grom on 5/11/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import SwiftUI

struct DealsView: View {
	
	@ObservedObject var viewModel = DealsViewModel()
	
	var body: some View {
		NavigationView{
			ZStack(alignment: .top){
				Background(color:.lightNavy)
				
				VStack(spacing: 0){
					
					header
					
					ScrollView{
						VStack(spacing: 0){
							ForEach(viewModel.selectedContracts, id: \.id){ contract in
								ContactViewCell(contract: contract)
							}
						}
						.padding(.horizontal, 25)
						.padding(.top, 12)
					}
					
					Spacer()
				}
			}
			.edgesIgnoringSafeArea(.top)
			.hiddenNavigationBarStyle()
		}
	.animation(nil)
	}
	
	var header: some View{
		Group{
			HeaderView(title: TextKeys.DealsView.title)
			CustomTabView(tabs: viewModel.tabs, selectedIndex: $viewModel.selectedTabIndex)
				.padding(.horizontal, 45)
				.padding(.top, 18)
		}
		.background(Color.lightNavyThree)
	}
}

struct DealsView_Previews: PreviewProvider {
    static var previews: some View {
        DealsView()
    }
}
