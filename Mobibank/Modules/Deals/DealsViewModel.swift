//
//  DealsViewModel.swift
//  Mobibank
//
//  Created by Grom on 5/11/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import Foundation

class DealsViewModel: ObservableObject{
	let tabs = [TextKeys.DealsView.allContracts, TextKeys.DealsView.pendingContracts]
	@Published var selectedTabIndex: Int = 0
	@Published var contracts = [Contract(name: "Utility contract - helsingin energia", type: "Personal"),
								Contract(name: "direct debit contract - telia oyj", type: "Personal"),
								Contract(name: "Apartment lease - sanni saarikoski", type: "Personal"),
								Contract(name: "Consulting agreement - aves LTD", type: "Mobihai LTD"),
								Contract(name: "letter of credit - nordea abp", type: "Overseas sales LTD"),
								Contract(name: "Sales of good (FOB) - Xiangmen stee…", type: "Overseas sales LTD"),
								Contract(name: "verokortti - tax administration", type: "Personal"),
								Contract(name: "Letter of credit  - Nordea ABP", type: "Overseas sales LTD", date: "Sent 24.09.2019 at 14:54")]
	
	var selectedContracts: [Contract]{
		selectedTabIndex == 0 ? contracts.filter{$0.date == nil} : contracts.filter{$0.date != nil}
	}
	
}
