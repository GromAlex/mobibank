//
//  Contract.swift
//  Mobibank
//
//  Created by Grom on 5/12/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import Foundation

struct Contract: Identifiable {
	let id = UUID()
	let name: String
	let type: String
	var date: String? = nil
}
