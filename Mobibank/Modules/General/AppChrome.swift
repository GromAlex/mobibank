//
//  AppChrome.swift
//  Mobibank
//
//  Created by Grom on 5/8/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import SwiftUI

struct AppChrome: View {
	
	@EnvironmentObject var appState: AppState
	
	var body: some View {
		ZStack{
			content
				.overlay(Color.black.opacity(appState.showMenu ? 0.4 : 0))
				.blur(radius: appState.showMenu ? 5.7 : 0)
				.disabled(appState.showMenu)
			
			
			Menu()
		}
		.sheet(isPresented: $appState.showProfile){
			ProfileView()
		}
	}
	
	var content: some View{
		if  self.appState.selectedMenuItem.withTabBar{
			return AnyView(UIKitTabView([
				UIKitTabView.Tab(view: HomeView(),
								 barItem: UITabBarItem(title: TextKeys.AppChrome.home,
													   image: UIImage(named: Assets.Icons.home),
													   selectedImage: nil)),
				UIKitTabView.Tab(view: CreditAndLoansView(),
								 barItem: UITabBarItem(title: TextKeys.AppChrome.creditsAndLoans,
													   image: UIImage(named: Assets.Icons.creditLoans),
													   selectedImage: nil)),
				UIKitTabView.Tab(view: DealsView(),
								 barItem: UITabBarItem(title: TextKeys.AppChrome.deals,
													   image: UIImage(named: Assets.Icons.deal),
													   selectedImage: nil)),
				UIKitTabView.Tab(view: TransfersView(),
								 barItem: UITabBarItem(title: TextKeys.AppChrome.transfers,
													   image: UIImage(named: Assets.Icons.investment),
													   selectedImage: nil)),
				UIKitTabView.Tab(view: BillingsView(),
								 barItem: UITabBarItem(title: TextKeys.AppChrome.billings,
													   image: UIImage(named: Assets.Icons.billings),
				selectedImage: nil))], selectedIndex: $appState.tabBarSelectedIndex))
		}
		else{
			return AnyView(destinationForSelectedMenu)
		}
	}
	
	var destinationForSelectedMenu: some View{
		switch self.appState.selectedMenuItem{
		case .investments: return AnyView(InvestmentsView())
		default: return AnyView(EmptyView())
		}
	}
}

struct AppChrome_Previews: PreviewProvider {
    static var previews: some View {
        AppChrome()
    }
}
