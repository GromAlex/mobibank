//
//  Background.swift
//  Mobibank
//
//  Created by Grom on 5/7/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import SwiftUI

struct Background: View {
	
	var color: Color = .cobalt
	var withPoints: Bool = false
	var withLogo: Bool = false
	
    var body: some View {
		ZStack{
			color.edgesIgnoringSafeArea(.all)
			if withPoints{
				Image(Assets.Backgrounds.points).resizable().aspectRatio(contentMode: .fit)
			}
			if withLogo{
				Image(Assets.Icons.logoBig)
			}
		}
    }
}

struct Background_Previews: PreviewProvider {
    static var previews: some View {
        Background( withPoints: true)
    }
}
