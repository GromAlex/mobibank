//
//  HeaderView.swift
//  Mobibank
//
//  Created by Grom on 5/11/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import SwiftUI

enum HeaderViewType{
	case root
	case pushed
	case modal
}

struct HeaderView: View {
	
	@EnvironmentObject var appState: AppState
	@Environment(\.presentationMode) private var presentationMode: Binding<PresentationMode>
	
	var title: String? = nil
	var type: HeaderViewType = .root
	var background: Color = .lightNavyThree
	
	var body: some View {
		VStack(alignment: .center){
			HStack{
				
				if type == .root{
					Button(action:{
						withAnimation{
							self.appState.showMenu.toggle()
						}
					}){
						Image(Assets.Icons.menu)
					}
					.buttonStyle(PlainButtonStyle())
				}
				else if type == .pushed{
					Button(action:{
						withAnimation{
							self.presentationMode.wrappedValue.dismiss()
						}
					}){
						Image(Assets.Icons.backArrow)
					}
					.buttonStyle(PlainButtonStyle())
				}
				Spacer()
				
				Image(Assets.Icons.logo).scaleEffect(0.55)
				
				Spacer()
				
				if type == .root{
					Button(action:{
						self.appState.showProfile.toggle()
					}){
						Image(Assets.HomeView.avatar)
					}
					.buttonStyle(PlainButtonStyle())
				}
			}
			
			ZStack{
				Unwrap(title){
					Text($0.uppercased())
						.font(.custom(Fonts.montserratSemibold, size: 12))
						.foregroundColor(.white)
						.kerning(1.09)
				}
				
				if type == .modal{
					HStack{
						Spacer()
						Button(action: {
							self.presentationMode.wrappedValue.dismiss()
						}){
							Image(Assets.Icons.close)
						}
						.buttonStyle(PlainButtonStyle())
					}
				}
			}
		}
		.padding(.horizontal, 25)
		.padding(.top, type == .modal ? 20 : 42)
		.padding(.bottom, 11)
		.background(background)
	}
}

struct HeaderView_Previews: PreviewProvider {
    static var previews: some View {
        HeaderView(title: "sdf")
    }
}
