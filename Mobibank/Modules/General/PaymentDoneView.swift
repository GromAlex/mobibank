//
//  PaymentDoneView.swift
//  Mobibank
//
//  Created by Grom on 5/19/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import SwiftUI

struct PaymentDoneView: View {
	
	let title: String
	let description: String
	var isViewPayslip: Bool = false
	var isTemplate: Bool = false
	
    var body: some View {
		ZStack{
			Background(color: .lightNavy)
			VStack{
				HeaderView(title: title, type: .modal)
				
				Spacer()
				Image(Assets.Icons.tick)
				Text(description.uppercased())
					.font(.custom(Fonts.montserratSemibold, size: 12))
					.foregroundColor(.white)
					.kerning(1.09)
					.padding(.top, 54)
				Spacer()
				
				if isTemplate{
					Text(TextKeys.Transfers.template)
					.font(.custom(Fonts.montserratSemibold, size: 12))
					.foregroundColor(.brightAqua)
					.kerning(1.09)
					.padding(.bottom, 37)
				}
				
				if isViewPayslip{
					BigRoundedButton(title: TextKeys.Transfers.viewPayslip)
						.padding(.bottom, 70)
				}
			}
		}
		.edgesIgnoringSafeArea(.top)
	}
}
