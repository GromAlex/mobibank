//
//  AccountViewCell.swift
//  Mobibank
//
//  Created by Grom on 5/8/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import SwiftUI

struct AccountViewCell: View {
	let account: Account
    var body: some View {
		VStack{
			HStack{
				Text(account.value)
					.font(.custom(Fonts.montserratMedium, size: 24))
					.kerning(0.21)
					.foregroundColor(.white)
				Spacer()
				Image(Assets.Icons.send)
				Image(Assets.Icons.link).padding(.leading, 20)
			}
			HStack{
				Text(account.name.uppercased())
				.font(.custom(Fonts.montserratSemibold, size: 12))
				.kerning(1.09)
				.foregroundColor(.white)
				Spacer()
				Text(account.number.uppercased())
				.font(.custom(Fonts.montserratRegular, size: 12))
				.kerning(1.09)
				.foregroundColor(.white)
			}
		}
    }
}

struct AccountViewCell_Previews: PreviewProvider {
    static var previews: some View {
		AccountViewCell(account: Account(name: "Deposit account", value: "€ 10.456,77", number: "fi 32 4220 4222 0440 32"))
    }
}
