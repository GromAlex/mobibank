//
//  CardViewCell.swift
//  Mobibank
//
//  Created by Grom on 5/8/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import SwiftUI

struct CardViewCell: View {
	let card: Card
    var body: some View {
		VStack(spacing: 16){
			Image(card.image)
			HStack{
				Text(card.number).kerning(1.09)
				Spacer()
				Text(card.balance).kerning(1.09)
			}
			.font(.custom(Fonts.montserratSemibold, size: 12))
			.foregroundColor(.white)
		}
		.padding(.bottom, 15)
    }
}

struct CardViewCell_Previews: PreviewProvider {
    static var previews: some View {
        CardViewCell(card: Card(image: Assets.HomeView.visa, number: "**** **** **** 4487", balance: "€ 5.276,00"))
    }
}
