//
//  CreditViewCell.swift
//  Mobibank
//
//  Created by Grom on 5/8/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import SwiftUI

struct CreditViewCell: View {
	enum ValueAlignment{
		case top
		case bottom
	}
	
	let credit: CreditItem
	var valueAlignment: ValueAlignment = .bottom
	
    var body: some View {
		VStack{
			HStack{
				Text(credit.name.uppercased())
					.font(.custom(Fonts.montserratSemibold, size: 12))
					.foregroundColor(.white)
					.kerning(1.09)
				Spacer()
				if valueAlignment == .top{
					Text(credit.value)
						.font(.custom(Fonts.montserratSemibold, size: 15))
						.kerning(0.13)
						.foregroundColor(.white)
				}
			}
			HStack{
				Text(credit.description)
					.font(.custom(Fonts.montserratRegular, size: 12))
					.kerning(1.09)
					.foregroundColor(.white)
				
				Spacer()
				if valueAlignment == .bottom{
					Text(credit.value)
						.font(.custom(Fonts.montserratSemibold, size: 15))
						.kerning(0.13)
						.foregroundColor(.white)
				}
			}
		}
    }
}

struct CreditViewCell_Previews: PreviewProvider {
    static var previews: some View {
        CreditViewCell(credit: Credit(name: "fsf", description: "sdf", value: "sdf"))
    }
}
