//
//  HomeView.swift
//  Mobibank
//
//  Created by Grom on 5/8/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import SwiftUI

struct HomeView: View {
	@ObservedObject var viewModel = HomeViewModel()
	
	var body: some View {
		NavigationView{
			ZStack(alignment: .top){
				Background(color:.cobalt)
				
				VStack(spacing: 0){
					HeaderView(background: .clear).zIndex(1)
					
					ScrollView{
						VStack(spacing: 0){
							accounts
							cards
							loans
							credits
						}
						.padding(.bottom, 27)
					}
					.zIndex(0)
				}
				.edgesIgnoringSafeArea(.top)
				.hiddenNavigationBarStyle()
			}
		}
		.animation(nil)
	}
		
	var accounts: some View{
		VStack(spacing: 30){
			HStack{
				Text(TextKeys.Home.accounts)
					.font(.custom(Fonts.montserratRegular, size: 15))
					.foregroundColor(.white)
					.kerning(1.36)
				Spacer()
				Text(TextKeys.Home.seeAll)
					.font(.custom(Fonts.montserratSemibold, size: 12))
					.foregroundColor(.brightAqua)
					.kerning(1.09)
			}
			ForEach(viewModel.accounts, id:\.number){ account in
				AccountViewCell(account: account)
			}
		}
		.padding(.vertical, 12)
		.padding(.horizontal, 15)
		.overlay(
			RoundedRectangle(cornerRadius: 10.5)
                .stroke(Color.white, lineWidth: 1)
        )
		.padding(.bottom, 18)
		.padding(.horizontal, 15)
		.padding(.top, 1)
	}
	
	var cards: some View{
		VStack{
			HStack{
				Text(TextKeys.Home.cards)
					.font(.custom(Fonts.montserratRegular, size: 15))
					.foregroundColor(.white)
					.kerning(1.36)
				Spacer()
				Text(TextKeys.Home.seeAll)
					.font(.custom(Fonts.montserratSemibold, size: 12))
					.foregroundColor(.brightAqua)
					.kerning(1.09)
			}
			.padding(.horizontal, 25)
			.padding(.top, 17)
			ScrollView(.horizontal){
				HStack(spacing: 16){
					ForEach(viewModel.cards, id:\.number){ card in
						CardViewCell(card: card)
					}
				}
				.padding(.horizontal, 25)
			}
			.padding(.top, 12)
		}
		//.background(Color.lightNavyTwo)
	}
	
	var loans: some View{
		VStack{
			HStack{
				Text(TextKeys.Home.loans)
					.font(.custom(Fonts.montserratRegular, size: 15))
					.foregroundColor(.white)
					.kerning(1.36)
				Spacer()
				Text(TextKeys.Home.more)
					.font(.custom(Fonts.montserratSemibold, size: 12))
					.foregroundColor(.brightAqua)
					.kerning(1.09)
			}

			CreditViewCell(credit: viewModel.loans.first!).padding(.top, 10)
		}
		.padding(10)
		.overlay(
			RoundedRectangle(cornerRadius: 10.5)
				.stroke(Color.white, lineWidth: 1)
		)
		.padding(.horizontal, 15)
		.padding(.top, 25)
	}
	
	var credits: some View{

		VStack{
			HStack{
				Text(TextKeys.Home.credits)
					.font(.custom(Fonts.montserratRegular, size: 15))
					.foregroundColor(.white)
					.kerning(1.36)
				Spacer()
				Text(TextKeys.Home.more)
					.font(.custom(Fonts.montserratSemibold, size: 12))
					.foregroundColor(.brightAqua)
					.kerning(1.09)
			}
			
			CreditViewCell(credit: viewModel.credits.first!).padding(.top, 10)
		}
		.padding(10)
		.overlay(
			RoundedRectangle(cornerRadius: 10.5)
				.stroke(Color.white, lineWidth: 1)
		)
		.padding(.horizontal, 15)
		.padding(.top, 10)
	}
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
