//
//  HomeViewModel.swift
//  Mobibank
//
//  Created by Grom on 5/8/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import Foundation
import Combine

class HomeViewModel: ObservableObject{
	@Published var accounts: [Account] =
		[Account(name: "Deposit account", value: "€ 10.456,77", number: "fi 32 4220 4222 0440 32"),
		 Account(name: "Salary account", value: "€ 5.276,00", number: "fi 54 4553 3533 2323 34")]
	
	@Published var cards = [Card(image: Assets.HomeView.visa, number: "**** **** **** 4487", balance: "€ 5.276,00"),
							Card(image: Assets.HomeView.applePay, number: "**** **** **** 4488", balance: "€ 314,00")]
	@Published var loans = [Loan(name: "Fellow credit", description: "Interest 12.00%", value: "€ 442,40", interest: "")]
	@Published var credits = [Credit(name: "Crypto credit", description: "r3kmLJN5D28dHuH8v…", value: "€ 2.000,00")]
}
