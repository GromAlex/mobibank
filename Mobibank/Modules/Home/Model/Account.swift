//
//  Account.swift
//  Mobibank
//
//  Created by Grom on 5/8/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import Foundation

struct Account{
	let name: String
	let value: String
	let number: String
}
