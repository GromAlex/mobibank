//
//  Card.swift
//  Mobibank
//
//  Created by Grom on 5/8/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import Foundation

struct Card{
	let image: String
	let number: String
	let balance: String
}
