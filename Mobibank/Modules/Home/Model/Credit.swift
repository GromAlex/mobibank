//
//  Credit.swift
//  Mobibank
//
//  Created by Grom on 5/8/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import Foundation

struct Credit: CreditItem{
	var id = UUID()
	let name: String
	let description: String
	let value: String
}
