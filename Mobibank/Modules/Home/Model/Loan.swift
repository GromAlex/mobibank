//
//  Loan.swift
//  Mobibank
//
//  Created by Grom on 5/8/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import Foundation

protocol CreditItem{
	var name: String { get }
	var description: String { get }
	var value: String { get }
	var id: UUID { get set}
}


struct Loan: CreditItem{
	var id = UUID()
	let name: String
	let description: String
	let value: String
	let interest: String
}
