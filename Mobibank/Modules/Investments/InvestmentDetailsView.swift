//
//  InvestmentDetailsView.swift
//  Mobibank
//
//  Created by Grom on 5/12/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import SwiftUI

struct InvestmentDetailsView: View {
	
	let title: String
	let investment: InvestmentItem
	
	var body: some View {
		ZStack{
			Background(color: .lightNavy)
			
			VStack(spacing: 0){
				HeaderView(title: title, type: .pushed)
				
				ScrollView{
					VStack{
						investmentName
						
						Image(Assets.InvestmentDetailsView.diagram)
							.resizable()
							.aspectRatio(contentMode: .fit)
							.padding(.top, 51)
							.padding(.bottom, 35)
						
						buttons
						
						BlockTitle(title: title)
							.padding(.top, 43)
							.padding(.bottom, 10)
						
						
						InvestmentDetailsViewCell(investment: investment)
						
						Spacer()
					}
					.padding(.horizontal, 25)
				}
			}
		}
		.edgesIgnoringSafeArea(.top)
		.hiddenNavigationBarStyle()
	}
	
	var investmentName: some View{
		HStack(spacing: 14){
			Circle().fill(Color.brightAqua)
				.frame(width: 10, height: 10)
			Text(investment.name.uppercased())
				.font(.custom(Fonts.montserratSemibold, size: 12))
				.kerning(1.09)
				.foregroundColor(.white)
			Spacer()
		}
		.padding(.top, 25)
	}
	
	var buttons: some View{
		HStack(spacing: 10){
			
			Button(action:{}){
				SellButton(title: TextKeys.InvestmentsView.buy, icon: Assets.Icons.bigArrowUp)
			}
			Button(action:{}){
				SellButton(title: TextKeys.InvestmentsView.sell, icon: Assets.Icons.bigArrowDown)
			}
		}
		.buttonStyle(PlainButtonStyle())
	}
	
	
	struct SellButton: View{
		
		let title: String
		let icon: String
		
		var body: some View{
			HStack(spacing: 30){
				Text(title.uppercased())
					.font(.custom(Fonts.montserratSemibold, size: 12))
					.foregroundColor(.white)
					.kerning(1.09)
				
				Image(icon)
			}
			.padding(.horizontal, 20)
			.frame(height: 50)
			.overlay(
				RoundedRectangle(cornerRadius: 50)
					.stroke(Color.white, lineWidth: 1))
		}
	}
}

struct InvestmentDetailsView_Previews: PreviewProvider {
	static var previews: some View {
		InvestmentDetailsView(title: "stock details", investment: Stock(name: "Sievi Kapital", description: "Sievi", value: "€ 2.000,00", shares: "2342", growthIntervals: [GrowthInterval(period: "24h", percent: 2.46), GrowthInterval(period: "7 days", percent: -1.21)]))
	}
}


