//
//  InvestmentDetailsViewCell.swift
//  Mobibank
//
//  Created by Grom on 5/15/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import SwiftUI

struct InvestmentDetailsViewCell: View {
	let investment: InvestmentItem
	
	var body: some View {
		VStack(alignment: .leading, spacing: 0){
			HStack{
				Text(investment.name.uppercased())
					.font(.custom(Fonts.montserratSemibold, size: 12))
					.foregroundColor(.white)
					.kerning(1.09)
				Spacer()
				
				Text(investment.value)
					.font(.custom(Fonts.montserratSemibold, size: 15))
					.kerning(0.13)
					.foregroundColor(.white)
			}
			.padding(.top, 13)
			
			HStack{
				Text(investment.description)
					.font(.custom(Fonts.montserratRegular, size: 12))
					.kerning(1.09)
					.foregroundColor(.white)
					.padding(.bottom, 9)
				Spacer()
				Text(investment.shares)
					.font(.custom(Fonts.montserratRegular, size: 12))
					.kerning(1.09)
					.foregroundColor(.white)
					.padding(.bottom, 9)
			}
			
			HStack{
				GrowthIntervals(intervals: investment.growthIntervals)
				Spacer()
				buttons
			}
		}
		.contentShape(Rectangle())
	}
	
	var buttons: some View{
		HStack{
			Button(action: {}){
				RoundedButton(title: TextKeys.InvestmentDetailsView.limitOrder)
			}
			
			Button(action: {}){
				RoundedButton(title: TextKeys.InvestmentDetailsView.setAlerts)
			}
		}
		.buttonStyle(PlainButtonStyle())
	}
}

struct InvestmentDetailsViewCell_Previews: PreviewProvider {
	static var previews: some View {
		InvestmentDetailsViewCell(investment: Fund(name: "FIM European Hy ESG", description: "Fim Varainhoito Oy", value: "€ 2.000,00", shares: "345345", growthIntervals: [GrowthInterval(period: "30 days", percent: 2.74)]))
	}
}
