//
//  InvestmentViewCell.swift
//  Mobibank
//
//  Created by Grom on 5/12/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import SwiftUI

struct InvestmentViewCell: View {
	
	let investment: InvestmentItem
	
	var body: some View {
		VStack(alignment: .leading, spacing: 0){
			HStack{
				Text(investment.name.uppercased())
					.font(.custom(Fonts.montserratSemibold, size: 12))
					.foregroundColor(.white)
					.kerning(1.09)
				Spacer()
				
				Text(investment.value)
					.font(.custom(Fonts.montserratSemibold, size: 15))
					.kerning(0.13)
					.foregroundColor(.white)
				
				Image(Assets.Icons.moreArrow)
				
			}
			.padding(.top, 13)
			
			Text(investment.description)
				.font(.custom(Fonts.montserratRegular, size: 12))
				.kerning(1.09)
				.foregroundColor(.white)
				.padding(.bottom, 9)
			
			HStack{
				GrowthIntervals(intervals: investment.growthIntervals)
				Spacer()
				buttons
			}
			
			Divider().frame(height: 1).background(Color.white10).padding(.top, 10)
		}
		.contentShape(Rectangle())
	}
	
	var buttons: some View{
		HStack{
			RoundedButton(title: TextKeys.InvestmentsView.pay, icon: Assets.Icons.arrowUp)
			RoundedButton(title:  TextKeys.InvestmentsView.apply, icon: Assets.Icons.arrowDown)
		}
	}
}

struct GrowthIntervals: View{
	
	let intervals: [GrowthInterval]
	
	var body: some View{
		VStack{
			ForEach(intervals, id: \.self){ interval in
				HStack{
					Text(interval.period)
						.font(.custom(Fonts.montserratRegular, size: 12))
						.kerning(1.09)
						.foregroundColor(.white)
						.fixedSize()
					
					Text(self.percentText(interval.percent))
						.font(.custom(Fonts.montserratRegular, size: 12))
						.kerning(1.09)
						.foregroundColor(interval.percent > 0 ? .appleGreen : .redPink )
						.fixedSize()
					
					Spacer()
				}
			}
		}
	}
	
	func percentText(_ percent: Float)->String{
		return "\(percent > 0 ? "+" : "")\(percent.description)%"
	}
}

struct InvestmentViewCell_Previews: PreviewProvider {
    static var previews: some View {
		InvestmentViewCell(investment: Fund(name: "FIM European Hy ESG", description: "Fim Varainhoito Oy", value: "€ 2.000,00", shares: "345345", growthIntervals: [GrowthInterval(period: "30 days", percent: 2.74)]))
    }
}
