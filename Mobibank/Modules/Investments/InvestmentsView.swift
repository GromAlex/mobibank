//
//  InvestmentsView.swift
//  Mobibank
//
//  Created by Grom on 5/12/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import SwiftUI

struct InvestmentsView: View {
	@ObservedObject var viewModel = InvestmentsViewModel()
   
	var body: some View {
		NavigationView{
			ZStack(alignment: .top){
				Background(color:.lightNavy)
				
				VStack(spacing: 0){
					
					header
					
					ScrollView{
						VStack(spacing: 0){
							ForEach(viewModel.selectedInvestments, id: \.id){ investment in
								NavigationLink(destination: InvestmentDetailsView(title: self.viewModel.detailsTitle, investment: investment)){
									InvestmentViewCell(investment: investment)
								}
								.buttonStyle(PlainButtonStyle())
							}
						}
						.padding(.horizontal, 25)
						.padding(.top, 15)
					}
					
					Spacer()
					
					BigRoundedButton(title: TextKeys.InvestmentsView.addToPortfolio).padding(.vertical, 40)
				}
			}
			.edgesIgnoringSafeArea(.top)
			.hiddenNavigationBarStyle()
		}
	}
	
	var header: some View{
		Group{
			HeaderView(title: TextKeys.InvestmentsView.title)
			CustomTabView(tabs: viewModel.tabs, selectedIndex: $viewModel.selectedTabIndex)
				.padding(.horizontal, 45)
				.padding(.top, 18)
		}
		.background(Color.lightNavyThree)
	}
}

struct InvestmentsView_Previews: PreviewProvider {
    static var previews: some View {
        InvestmentsView()
    }
}
