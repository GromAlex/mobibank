//
//  InvestmentsViewModel.swift
//  Mobibank
//
//  Created by Grom on 5/12/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import Foundation

class InvestmentsViewModel: ObservableObject{
	let tabs = [TextKeys.InvestmentsView.stockPortfolio, TextKeys.InvestmentsView.funds]
	@Published var selectedTabIndex: Int = 0
	@Published var stocks = [Stock(name: "Sievi Kapital", description: "Sievi", value: "€ 2.000,00", shares: "8.9158 shares",  growthIntervals: [GrowthInterval(period: "24h", percent: 2.46), GrowthInterval(period: "7 days", percent: -1.21)]),
							 Stock(name: "Kemira", description: "Kemira", value: "€ 4.500,00", shares: "6.7158 shares", growthIntervals: [GrowthInterval(period: "24h", percent: 1.12), GrowthInterval(period: "7 days", percent: 3.17)]),
							 Stock(name: "Vaisala a a", description: "VA | AS", value: "€ 8.000,00", shares: "3.3434 shares", growthIntervals: [GrowthInterval(period: "24h", percent: -1.85), GrowthInterval(period: "7 days", percent: 0.11)]),
							 Stock(name: "Telenom", description: "TNOM", value: "€ 8.000,00", shares: "7.3434 shares", growthIntervals: [GrowthInterval(period: "24h", percent: 7.46), GrowthInterval(period: "7 days", percent: 3.12)])]
	
	@Published var funds = [Fund(name: "FIM European Hy ESG", description: "Fim Varainhoito Oy", value: "€ 2.000,00", shares: "6.2121 shares", growthIntervals: [GrowthInterval(period: "30 days", percent: 2.74), GrowthInterval(period: "1a", percent: 4.16)]),
							Fund(name: "First Trust indxx inn", description: "LEGR", value: "€ 4.500,00", shares: "9.9999 shares", growthIntervals: [GrowthInterval(period: "30 days", percent: -0.21), GrowthInterval(period: "1a", percent: -1.36)])]
	
	var selectedInvestments: [InvestmentItem]{
		selectedTabIndex == 0 ? stocks : funds
	}
	
	var detailsTitle: String{
		String(format: TextKeys.InvestmentDetailsView.title, selectedTabIndex == 0 ? TextKeys.InvestmentsView.stocks :  TextKeys.InvestmentsView.funds)
	}
}
