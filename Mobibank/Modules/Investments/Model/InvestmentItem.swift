//
//  InvestmentItem.swift
//  Mobibank
//
//  Created by Grom on 5/12/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import Foundation

struct GrowthInterval: Hashable{
	let period: String
	let percent: Float
}

protocol InvestmentItem{
	var name: String { get }
	var description: String { get }
	var value: String { get }
	var shares: String { get}
	var growthIntervals: [GrowthInterval] { get }
	var id: UUID { get set}
}


struct Stock: InvestmentItem {
	var name: String
	var description: String
	var value: String
	var shares: String
	var growthIntervals: [GrowthInterval]
	var id: UUID = UUID()
}

struct Fund: InvestmentItem {
	var name: String
	var description: String
	var value: String
	var shares: String
	var growthIntervals: [GrowthInterval]
	var id: UUID = UUID()
}
