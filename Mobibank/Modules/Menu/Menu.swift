//
//  Menu.swift
//  Mobibank
//
//  Created by Grom on 5/12/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import SwiftUI

struct Menu: View {
	
	@EnvironmentObject var appState: AppState
	@ObservedObject var viewModel = MenuViewModel()
	
	var body: some View {
		
		GeometryReader { geometry in
			ZStack(alignment: .leading){
				Background(color: .cobalt)
				
				VStack(alignment: .leading){
					
					HStack(spacing: 0){
						Button(action:{
							withAnimation{
								self.appState.showMenu.toggle()
							}
						}){
							Image(Assets.Icons.backArrow)
						}
						.buttonStyle(PlainButtonStyle())
						
						Image(Assets.Icons.logo).scaleEffect(0.55).padding(.leading, 36)
						
						Spacer()
					}
					.padding(.leading, 25)
					.padding(.top, 48)
					
					ScrollView(showsIndicators: false){
						self.buttons
					}
				}
				.offset(x: geometry.size.width * self.viewModel.openMultiplier)

			}
			.offset(x:  -geometry.size.width * (self.appState.showMenu ? self.viewModel.openMultiplier : 1))
			.gesture(DragGesture()
			.onEnded {
				
				let translation = $0.translation.width
				
				if abs(translation) > 50 {
					withAnimation {
						self.appState.showMenu = translation > 0 ? true : false
					}
				}
			})
		}
		.edgesIgnoringSafeArea(.top)
	}
	
	var buttons: some View{
		Group{
			buttons(for: .top).padding(.top, 52)
			Spacer()
			buttons(for: .bottom).padding(.bottom, 47)
		}
		.padding(.leading, 35)
	}
	
	func buttons(for alignment: MenuItemAlignment)-> some View {
		VStack(alignment: .leading){
			ForEach(MenuItem.allCases.filter{$0.alignment == alignment}, id: \.self){ item in
				
				//HStack{
				Button(action: {
					withAnimation{
						self.appState.selectedMenuItem = item
						self.appState.showMenu.toggle()
					}
				}){
				Text(item.title.uppercased())
					.font(.custom(Fonts.montserratMedium, size: 12.5))
					.foregroundColor(item.isEnabled ? .white : .gray)
					.kerning(1.46)
					.padding(.vertical, 15)
				}
				.disabled(!item.isEnabled)
					//Spacer()
				//}
			}
		}
		//.background(Color.red)
	}
}
