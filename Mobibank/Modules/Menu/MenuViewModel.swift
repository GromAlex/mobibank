//
//  MenuViewModel.swift
//  Mobibank
//
//  Created by Grom on 5/12/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import Foundation
import UIKit

enum MenuItemAlignment{
	case top
	case bottom
}

enum MenuItem: CaseIterable{
	case home
	case creditsAndLoans
	case deals
	case transfers
	case billings
	case investments
	case wallets
	case trading
	case criptoCredit
	case preferencies
	case customerSupport
	case lostCard
	case signOut
	
	var title: String{
		switch self{
		case .home: return TextKeys.Menu.home
		case .creditsAndLoans: return TextKeys.Menu.creditsAndLoans
		case .deals: return TextKeys.Menu.deals
		case .transfers: return TextKeys.Menu.transfers
		case .billings: return TextKeys.Menu.billings
		case .investments: return TextKeys.Menu.investments
		case .wallets: return TextKeys.Menu.wallets
		case .trading: return TextKeys.Menu.trading
		case .criptoCredit: return TextKeys.Menu.criptoCredit
		case .preferencies: return TextKeys.Menu.preferencies
		case .customerSupport: return TextKeys.Menu.customerSupport
		case .lostCard: return TextKeys.Menu.lostCard
		case .signOut: return TextKeys.Menu.signOut
		}
	}
	
	var alignment: MenuItemAlignment{
		switch self {
		//case .lostCard, .signOut:
		//	return .bottom
		default:
			return .top
		}
	}
	
	var withTabBar: Bool{
		switch self{
		case  .home, .creditsAndLoans, .deals, .transfers, .billings: return true
		default: return false
		}
	}
	
	var tabBarIndex: Int?{
		switch self{
		case .home: return 0
		case .creditsAndLoans: return 1
		case .deals: return 2
		case .transfers: return 3
		case .billings: return 4
		default: return nil
		}
	}
	
	var isEnabled: Bool{
		switch self{
		case  .home, .creditsAndLoans, .deals, .transfers, .billings, .investments: return true
		default: return false
		}
	}
	
//	var destination: some View{
//		switch self{
//			case .home: return AnyView(HomeView())
//			case .creditsAndLoans: return AnyView(CreditAndLoansView())
//			case .deals: return TextKeys.Menu.deals
//			case .transfers: return TextKeys.Menu.transfers
//			case .billings: return TextKeys.Menu.billings
//			case .investments: return TextKeys.Menu.investments
//			case .wallets: return TextKeys.Menu.wallets
//			case .trading: return TextKeys.Menu.trading
//			case .criptoCredit: return TextKeys.Menu.criptoCredit
//			case .preferencies: return TextKeys.Menu.preferencies
//			case .customerSupport: return TextKeys.Menu.customerSupport
//			case .lostCard: return TextKeys.Menu.lostCard
//			case .signOut: return TextKeys.Menu.signOut
//		}
//	}
}

class MenuViewModel: ObservableObject{
	let openMultiplier: CGFloat = 0.2
}
