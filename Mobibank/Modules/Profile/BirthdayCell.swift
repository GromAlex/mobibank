//
//  BirthdayCell.swift
//  Mobibank
//
//  Created by Grom on 5/13/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import SwiftUI

struct BirthdayCell: View {
	
	let date: String
	
	var body: some View {
		
		VStack(alignment: .leading, spacing: 0){
			
			Divider().frame(height: 1).background(Color.white10)
			
			HStack(alignment: .center){
				VStack(alignment: .leading, spacing: 0){
					Text(TextKeys.ProfileView.birthDate)
						.font(.custom(Fonts.montserratRegular, size: 12))
						.foregroundColor(.white)
						.kerning(1.09)
						.padding(.bottom, 7)
					
					Text(date.uppercased())
						.font(.custom(Fonts.montserratSemibold, size: 12))
						.foregroundColor(.white)
						.kerning(1.09)
				}
				
				Spacer()
				
				Image(Assets.Icons.edit)
				
			}
			.padding(.vertical, 12.5)
			
			Divider().frame(height: 1).background(Color.white10)
		}
	}
}

struct BirthdayCell_Previews: PreviewProvider {
	static var previews: some View {
		BirthdayCell(date: "sdf")
	}
}
