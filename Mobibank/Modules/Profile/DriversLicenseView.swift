//
//  DriversLicenseView.swift
//  Mobibank
//
//  Created by Grom on 5/13/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import SwiftUI

struct DriversLicenseView: View {
    var body: some View {
       ZStack{
			Background(color: .lightNavy)
			
			VStack{
				Image(Assets.ProfileView.drivingLicense)
				
				Spacer()

			}
			.padding(.top, 56)
		}
		.navigationBarTitle(LocalizedStringKey(TextKeys.ProfileView.driversLicense.uppercased()), displayMode: .inline)
		.navigationBarItems(trailing: Image(Assets.Icons.edit))
    }
}

struct DriversLicenseView_Previews: PreviewProvider {
    static var previews: some View {
        DriversLicenseView()
    }
}
