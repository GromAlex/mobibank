//
//  Profile.swift
//  Mobibank
//
//  Created by Grom on 5/12/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import Foundation

struct Profile{
	let firstName: String
	let lastName: String
	let address: String
	let birthDate: String
}
