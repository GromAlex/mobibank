//
//  ProfileItemCell.swift
//  Mobibank
//
//  Created by Grom on 5/13/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import SwiftUI

struct ProfileItemCell: View {
	let title: String
	
	var body: some View {
		VStack(spacing: 0){
			HStack{
				Text(title.uppercased())
					.font(.custom(Fonts.montserratSemibold, size: 12))
					.foregroundColor(.white)
					.kerning(1.09)
				Spacer()
				Image(Assets.Icons.moreArrow)
			}
			.padding(.top, 20.5)
			
			Divider().frame(height: 1).background(Color.white10).padding(.top, 20.5)
		}
	}
}

struct ProfileItemCell_Previews: PreviewProvider {
    static var previews: some View {
		ProfileItemCell(title: "sdf")
    }
}
