//
//  ProfileView.swift
//  Mobibank
//
//  Created by Grom on 5/12/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import SwiftUI

struct ProfileView: View {
	
	@ObservedObject var viewModel = ProfileViewModel()
	@Environment(\.presentationMode) private var presentationMode: Binding<PresentationMode>
	
	var body: some View {
		NavigationView{
			ZStack{
				Background(color: .lightNavyThree)
				VStack{
					
					//HeaderView(title: TextKeys.ProfileView.title)
					
					avatar.padding(.bottom, 43)
					
					ScrollView{
						VStack(alignment: .leading, spacing: 0){
							BirthdayCell(date: viewModel.profile.birthDate)
							ForEach(ProfileMenuItem.allCases.filter{$0 != .birthDate}, id: \.self){
								self.cell($0)
							}
						}
						.padding(.horizontal, 25)
						
						Spacer()
					}
					Spacer()
					
					BigRoundedButton(title: TextKeys.ProfileView.addMoreDocument).padding(.bottom, 20)
				}
				.navigationBarTitle(LocalizedStringKey(TextKeys.ProfileView.title.uppercased()), displayMode: .inline)
				.navigationBarItems(trailing: closeButton)
				
				Unwrap(self.viewModel.selectedItem){
					NavigationLink(destination: $0.destination, isActive: self.$viewModel.isActive) {
						EmptyView()
					}
				}
			}
			//.hiddenNavigationBarStyle()
			//.edgesIgnoringSafeArea(.top)
		}
	}
	
	
	func cell(_ item: ProfileMenuItem)-> some View{
		Button(action: {
			self.viewModel.selectedItem = item
		}){
			ProfileItemCell(title: item.title)
			.contentShape(Rectangle())
		}
		.buttonStyle(PlainButtonStyle())
	}
	
	var avatar: some View{
		VStack{
			ZStack(alignment: .bottomTrailing){
				Image(Assets.ProfileView.bigAvatar)
				Image(Assets.Icons.smallEdit)
			}
			.padding(.top, 37)
			.padding(.bottom, 25)
			Text(viewModel.fullName.uppercased())
				.font(.custom(Fonts.montserratSemibold, size: 12))
				.foregroundColor(.white)
				.kerning(1.09)
				.padding(.bottom, 10)
			
			Text(viewModel.profile.address)
				.font(.custom(Fonts.montserratRegular, size: 12))
				.foregroundColor(.white)
				.kerning(1.09)
				.multilineTextAlignment(.center)
				.padding(.horizontal, 63)
		}
		.navigationBarItems(trailing: Image(Assets.Icons.edit))
	}
	
	var closeButton: some View{
		HStack{
			Spacer()
			Button(action:{
				self.presentationMode.wrappedValue.dismiss()
			}){
				Image(Assets.Icons.close)
			}
		.buttonStyle(PlainButtonStyle())
		}
	}
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView()
    }
}
