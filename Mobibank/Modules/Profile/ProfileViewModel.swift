//
//  ProfileViewModel.swift
//  Mobibank
//
//  Created by Grom on 5/12/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import Foundation
import SwiftUI

enum ProfileMenuItemType{
	case birthDate
	case other
}

enum ProfileMenuItem: CaseIterable{
	case birthDate
	case personalId
	case medicalId
	case passport
	case driversLicense
	
	var title: String{
		switch self{
		case .birthDate: return TextKeys.ProfileView.birthDate
		case .personalId: return TextKeys.ProfileView.personalId
		case .medicalId: return TextKeys.ProfileView.medicalId
		case .passport: return TextKeys.ProfileView.passport
		case .driversLicense: return TextKeys.ProfileView.driversLicense
		}
	}
	
	var destination: some View{
		switch self{
		case .driversLicense: return AnyView(DriversLicenseView())
		default: return AnyView(EmptyView())
		}
	}
	
	var isActive: Bool{
		switch self {
		case .driversLicense:
			return true
		default:
			return false
		}
	}
}



class ProfileViewModel: ObservableObject{
	let profile = Profile(firstName: "Jussi", lastName: "Teeriaho", address: "Ensonmaki 17 FIN-02440 Luoma, Finland", birthDate: "June 24, 1967")
	
	var selectedItem: ProfileMenuItem?{
		didSet{
			self.isActive = selectedItem?.isActive ?? false
		}
	}
	
	@Published var isActive: Bool = false
	
	var fullName: String{
		"\(profile.firstName) \(profile.lastName)"
	}
}
