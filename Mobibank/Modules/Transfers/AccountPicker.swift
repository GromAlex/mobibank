//
//  AccountPicker.swift
//  Mobibank
//
//  Created by Grom on 5/14/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import SwiftUI

struct AccountPicker: View{
	
	@Binding var accounts: [Account]
    @Binding var selection: Int
	
	var body: some View{
		HStack{
			Spacer()
			AccountPickerView(accounts: $accounts, selection: $selection)
			.colorInvert()
			.colorMultiply(Color.black)
			Spacer()
		}
		.background(Color.white)
	}
}
