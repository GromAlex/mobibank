//
//  UIKitPickerView.swift
//  Sihaty
//
//  Created by Grom on 4/1/20.
//  Copyright © 2020 Bamboo Agile. All rights reserved.
//

import SwiftUI


struct AccountPickerView: UIViewRepresentable {
	@Binding var accounts: [Account]
    @Binding var selection: Int


    func makeCoordinator() -> AccountPickerView.Coordinator {
        Coordinator(self)
    }

    func makeUIView(context: UIViewRepresentableContext<AccountPickerView>) -> UIPickerView {
		let picker = UIPickerView()
        picker.delegate = context.coordinator
        return picker
    }

    
    func updateUIView(_ view: UIPickerView, context: UIViewRepresentableContext<AccountPickerView>) {
		context.coordinator.accounts = accounts
		view.selectRow(self.selection, inComponent: 0, animated: false)
		view.reloadAllComponents()
    }

	class Coordinator: NSObject, UIPickerViewDataSource, UIPickerViewDelegate {
        var parent: AccountPickerView
		var accounts: [Account] = []
	
        init(_ pickerView: AccountPickerView) {
            self.parent = pickerView
        }

		func numberOfComponents(in pickerView: UIPickerView) -> Int {
			return 1
		}
		
		func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
			return self.accounts.count
		}
		
		func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
			
			var stackView: UIStackView
			
			if let v = view as? UIStackView{
				stackView = v
			}
			else{
				stackView = UIStackView()
			}
			

			stackView.axis = .vertical
			stackView.alignment = .center // .Leading .FirstBaseline .Center .Trailing .LastBaseline
			stackView.distribution = .fill // .FillEqually .FillProportionally .EqualSpacing .EqualCentering
			
			let nameLabel = UILabel(frame: CGRect.zero)
			nameLabel.font = UIFont(name: Fonts.montserratRegular, size: 24)
			nameLabel.text = self.accounts[row].name
			stackView.addArrangedSubview(nameLabel)
			
			let numberLabel = UILabel(frame: CGRect.zero)
			numberLabel.font = UIFont(name: Fonts.montserratRegular, size: 12)
			numberLabel.text = self.accounts[row].number
			stackView.addArrangedSubview(numberLabel)
			
			
			return stackView
		}
		
		func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
			self.parent.selection = row
		}
		
		func pickerView(_ pickerView: UIPickerView,
						rowHeightForComponent component: Int) -> CGFloat {
			return 52
		}
	}
}
