//
//  ChooseAccountView.swift
//  Mobibank
//
//  Created by Grom on 5/14/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import SwiftUI

struct ChooseAccountView: View{

	@Binding var showAccountPicker: Bool
	let account: Account
	
	var body: some View{

		VStack(alignment: .leading, spacing: 10){
			Text(TextKeys.Transfers.whichAccount.uppercased())
				.font(.custom(Fonts.montserratSemibold, size: 12))
				.foregroundColor(.white)
				.kerning(1.09)
			Button(action: {
				withAnimation{
					self.showAccountPicker.toggle()
				}
			}){
				HStack{
					VStack(alignment: .leading){
						Text(account.name.uppercased())
							.font(.custom(Fonts.montserratSemibold, size: 12))
							.foregroundColor(.white)

						Text(account.number)
							.font(.custom(Fonts.montserratRegular, size: 12))
							.foregroundColor(.white)
					}
					Spacer()
					Image(Assets.Icons.moreArrow).rotationEffect(.degrees(90))
				}
				.padding(.horizontal, 14)
				.frame(height: 50)
				.background(Color.lightNavyTwo)
				.cornerRadius(5)
			}
			.buttonStyle(PlainButtonStyle())
		}
	}
}
