//
//  ChooseDateView.swift
//  Mobibank
//
//  Created by Grom on 5/14/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import SwiftUI

struct ChooseDateView: View{
	
	@Binding var showDatePicker: Bool
	let date: String
	
	var body: some View{
		VStack(alignment: .leading, spacing: 10){
			Text(TextKeys.Transfers.dueDate.uppercased())
				.font(.custom(Fonts.montserratSemibold, size: 12))
				.foregroundColor(.white)
				.kerning(1.09)
			
			Button(action: {
				withAnimation{
					self.showDatePicker.toggle()
				}
			}){
				HStack{
					Text(date)
						.font(.custom(Fonts.montserratSemibold, size: 15))
						.foregroundColor(.white)
					Spacer()
					Image(Assets.Icons.date)
				}
				.padding(.horizontal, 14)
				.frame(height: 50)
				.background(Color.lightNavyTwo)
				.cornerRadius(5)
			}
			.buttonStyle(PlainButtonStyle())
		}
	}
}
