//
//  ChooseRecepientView.swift
//  Mobibank
//
//  Created by Grom on 5/15/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import SwiftUI

struct ChooseRecepientView: View {
	@Binding var showRecepientPicker: Bool
	let recepient: Recepient?
	
	var body: some View{
		VStack(alignment: .leading, spacing: 10){
			Text(TextKeys.Transfers.chooseRecepient.uppercased())
				.font(.custom(Fonts.montserratSemibold, size: 12))
				.foregroundColor(.white)
				.kerning(1.09)
			Button(action: {
				withAnimation{
					self.showRecepientPicker = true
				}
			}){
				HStack{
					
					Text(recepient?.name.uppercased() ?? "")
						.font(.custom(Fonts.montserratSemibold, size: 12))
						.foregroundColor(.white)
					
					Spacer()
					Image(Assets.Icons.account)
				}
				.padding(.horizontal, 14)
				.frame(height: 50)
				.background(Color.lightNavyTwo)
				.cornerRadius(5)
			}
			.buttonStyle(PlainButtonStyle())
		}
	}

}

