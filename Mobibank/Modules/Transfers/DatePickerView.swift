//
//  DatePickerView.swift
//  Mobibank
//
//  Created by Grom on 5/14/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import SwiftUI

struct DatePickerView: View{
	@Binding var date: Date
	
	var body: some View{
		HStack {
			Spacer()
			DatePicker("", selection: $date, displayedComponents: .date)
				.labelsHidden()
				.colorInvert()
				.colorMultiply(Color.black)
			Spacer()
		}
		.background(Color.white)
	}
}
 
