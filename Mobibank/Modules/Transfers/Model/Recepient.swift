//
//  Recepient.swift
//  Mobibank
//
//  Created by Grom on 5/15/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import Foundation

struct Recepient{
	let name: String
	let group: String
}
