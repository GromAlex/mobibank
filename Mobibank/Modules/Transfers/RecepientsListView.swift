//
//  RecepientsListView.swift
//  Mobibank
//
//  Created by Grom on 5/15/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import SwiftUI

struct RecepientsListView: View {
	
	@Environment(\.presentationMode) private var presentationMode: Binding<PresentationMode>
	
	let recepients:[Recepient]
	@Binding var selectedRecepient: Recepient?
	@Binding var show: Bool
	
    var body: some View {
		ZStack{
			VStack(alignment: .leading){
				ZStack{
					title
					closeButton
				}
				.padding(.top, 23)
					
				ScrollView{
					VStack(alignment: .leading){
						ForEach(groups, id: \.self){
							GroupView(name: $0, recepients: self.recepientsFor($0), selectedRecepient: self.$selectedRecepient)
						}
					}
					.padding(.horizontal, 25)
				}
				.padding(.top, 50)
			}
			//.padding(.horizontal, 25)
		}
		.background(RoundedView(tl: 42, tr: 42, bl: 0, br: 0).fill(Color.lightNavy))
		.animation(nil)
		.frame(height: UIScreen.main.bounds.height - 200)
    }
	
	var title: some View{
		HStack{
			Spacer()
			Text(TextKeys.Transfers.contacts.uppercased())
				.font(.custom(Fonts.montserratSemibold, size: 12))
				.foregroundColor(.white)
			Spacer()
		}
	}
	
	var closeButton: some View{
		HStack{
			Spacer()
			Button(action:{
				withAnimation{
					self.show.toggle()
				}
			}){
				Image(Assets.Icons.close)
			}
			.buttonStyle(PlainButtonStyle())
			.padding(.trailing, 20)
		}
	}
	
	var groups: [String]{
		Array(Set(recepients.map{$0.group})).sorted { $0 < $1 }
	}
	
	func recepientsFor(_ group: String)->[Recepient]
	{
		return recepients.filter{$0.group == group}
	}
	
	struct GroupView: View {
		let name: String
		let recepients:[Recepient]
		@Binding var selectedRecepient: Recepient?
		
		var body: some View{
			VStack(alignment: .leading, spacing: 20){
				Text(name.uppercased())
					.font(.custom(Fonts.montserratRegular, size: 15))
					.foregroundColor(.white)
					.kerning(1.36)
				
				ForEach(recepients, id:\.name){ recepient in
					HStack{
					Button(action:{
						self.selectedRecepient = recepient
					}){
						Text(recepient.name.uppercased())
							.font(.custom(Fonts.montserratSemibold, size: 12))
							.foregroundColor(.white)
							.kerning(1.09)
					}
						Spacer()
						
					}
				}
			}
			.padding(.vertical, 30)
		}
	}
}
