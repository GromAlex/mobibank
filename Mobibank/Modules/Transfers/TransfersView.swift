//
//  TransfersView.swift
//  Mobibank
//
//  Created by Grom on 5/14/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import SwiftUI

struct TransfersView: View {
	
	@ObservedObject var viewModel = TransfersViewModel()
	@Environment(\.presentationMode) private var presentationMode: Binding<PresentationMode>
	
	var body: some View {
		NavigationView{
			ZStack(alignment: .top){
				Background(color:.lightNavy)
				VStack{
					header
					ScrollView{
						VStack(spacing: 0){
							
							howMuchText
							
							LabelTextField(placeHolder: TextKeys.Transfers.amountPlaceholder, text: $viewModel.moneyValue, keyboardType: .numberPad)
								.padding(.top, 28)
								.padding(.bottom, 23)
							
							ChooseRecepientView(showRecepientPicker: $viewModel.showRecepientPicker, recepient: viewModel.selectedRecepient)
								.padding(.bottom, 20)
							
							ChooseAccountView(showAccountPicker: $viewModel.showAccountPicker, account: viewModel.selectedAccount)
								.padding(.bottom, 23)
							
							ChooseDateView(showDatePicker: $viewModel.showDatePicker, date: viewModel.dateFormatted)
								.padding(.bottom, 20)
							
							Button(action:{
								self.viewModel.showDone.toggle()
							}){
								BigRoundedButton(title: TextKeys.Transfers.transfer)
									.opacity(viewModel.buttonDisabled ? 0.4 : 1)
									.padding(.bottom, 50)
							}
							.disabled(viewModel.buttonDisabled)
							Spacer()
							
						}
						.padding(.horizontal, 25)
					}
				}
			}
			.edgesIgnoringSafeArea(.top)
			.hiddenNavigationBarStyle()
			.blur(radius: viewModel.showRecepientPicker ? 5.7 : 0)
			.disabled(viewModel.showRecepientPicker)
			.customBottomSheet(isPresented: $viewModel.showRecepientPicker){
				RecepientsListView(recepients: self.viewModel.recepients, selectedRecepient: self.$viewModel.selectedRecepient, show: self.$viewModel.showRecepientPicker)
			}
			.customBottomSheet(isPresented: $viewModel.showDatePicker){
				DatePickerView(date: self.$viewModel.date)
			}
			.customBottomSheet(isPresented: $viewModel.showAccountPicker){
				AccountPicker(accounts: self.$viewModel.accounts, selection: self.$viewModel.accountSelection)
			}
			.sheet(isPresented: $viewModel.showDone){
				PaymentDoneView(title: TextKeys.Transfers.title, description: TextKeys.Transfers.done, isViewPayslip: true, isTemplate: true)
			}
			.onReceive(viewModel.$selectedRecepient){_ in
				self.viewModel.showRecepientPicker = false
			}
		}
	}
	
	var header: some View{
		Group{
			HeaderView(title: TextKeys.Transfers.title)
		}
		.background(Color.lightNavyThree)
	}
	
	var howMuchText: some View{
		Text(TextKeys.Transfers.howMuch)
			.font(.custom(Fonts.montserratSemibold, size: 24))
			.foregroundColor(.white)
			.padding(.top, 25)
	}
}

struct TransfersView_Previews: PreviewProvider {
	static var previews: some View {
		TransfersView()
	}
}
