//
//  Assets.swift
//  Mobibank
//
//  Created by Grom on 5/7/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import Foundation

struct Assets {

}

//MARK: Backgrounds
extension Assets {
    struct Backgrounds {
        static let points = "points2"
    }
}

//MARK: Icons
extension Assets {
    struct Icons {
        static let backArrow        = "arrowLeft"
		static let logo             = "logo"
		static let logoBig          = "logoBig"
		static let faceId           = "faceId"
		static let wrong            = "wrong"
		static let correct          = "correct"
		static let delete           = "delete"
		static let home             = "home"
		static let creditLoans      = "creditLoans"
		static let deal             = "deal"
		static let investment       = "investment"
		static let billings         = "billings"
		static let menu             = "menu"
		static let link             = "link"
		static let send             = "send"
		static let arrowUp          = "arrowUp"
		static let arrowDown        = "arrowDown"
		static let bigArrowUp       = "bigArrowUp"
		static let bigArrowDown     = "bigArrowDown"
		static let arrowNext        = "arrowNext"
		static let moreArrow        = "moreArrow"
		static let edit             = "edit"
		static let defaultActive    = "defaultActive"
		static let defaultDisabled    = "defaultDisabled"
		static let date             = "date"
		static let account          = "account"
		static let close         	= "close"
		static let smallEdit        = "smallEdit"
		static let tick             = "tick"
	}
}

//MARK: HomeView
extension Assets {
	struct HomeView {
		static let avatar           = "avatar"
		static let visa             = "visa"
		static let applePay         = "applePay"
	}
}

//MARK: ProfileView
extension Assets{
	struct ProfileView {
		static let bigAvatar         = "bigAvatar"
		static let drivingLicense    = "drivingLicense"
	}
}

//MARK: InvestmentDetailsView
extension Assets{
	struct InvestmentDetailsView{
		static let diagram = "diagram"
	}
}
