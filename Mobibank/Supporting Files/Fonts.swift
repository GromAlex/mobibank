//
//  Fonts.swift
//  Mobibank
//
//  Created by Grom on 5/7/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import Foundation

struct Fonts {
	static let montserratThinItalic = "Montserrat-ThinItalic"
	static let montserratThin = "Montserrat-Thin"
	static let montserratMedium = "Montserrat-Medium"
	static let montserratMediumItalic = "Montserrat-MediumItalic"
	static let montserratBlack = "Montserrat-Black"
	static let montserratBlackItalic = "Montserrat-BlackItalic"
	static let montserratRegular = "Montserrat-Regular"
	static let montserratBoldItalic = "Montserrat-BoldItalic"
	static let montserratBold = "Montserrat-Bold"
	static let montserratItalic = "Montserrat-Italic"
	static let montserratExtraLight = "Montserrat-ExtraLight"
	static let montserratExtraLightItalic = "Montserrat-ExtraLightItalic"
	static let montserratExtraBold = "Montserrat-ExtraBold"
	static let montserratExtraBoldItalic = "Montserrat-ExtraBoldItalic"
	static let montserratSemibold = "Montserrat-SemiBold"
	static let montserratSemiboldItalic = "Montserrat-SemiBoldItalic"
}

