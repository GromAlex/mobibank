//
//  BigRoundedButton.swift
//  Mobibank
//
//  Created by Grom on 5/11/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import SwiftUI

struct BigRoundedButton: View {
	let title: String
	
	var body: some View {
		
		Text(title.uppercased())
			.font(.custom(Fonts.montserratSemibold, size: 12))
			.foregroundColor(.white)
			.kerning(1.09)
			.padding(.horizontal, 18)
			.frame(minWidth: 200, idealWidth: 200, maxWidth: 250, minHeight: 50, idealHeight: 50)
			.background(Color.turquoiseBlue)
			.cornerRadius(50)
		
	}
}

struct BigRoundedButton_Previews: PreviewProvider {
    static var previews: some View {
        BigRoundedButton(title: "sdf")
    }
}
