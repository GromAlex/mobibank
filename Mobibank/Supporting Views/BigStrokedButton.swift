//
//  BigStrokedBUtton.swift
//  Mobibank
//
//  Created by Grom on 5/13/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import SwiftUI

struct BigStrokedButton: View {
	
	let title: String
	
	var body: some View {
		Text(title.uppercased())
			.font(.custom(Fonts.montserratSemibold, size: 12))
			.foregroundColor(.white)
			.kerning(1.09)
			.padding(.horizontal, 30)
			.frame(minWidth: 200, idealWidth: 200, maxWidth: 250, minHeight: 50, idealHeight: 50)
			 .overlay(
				 RoundedRectangle(cornerRadius: 50)
					 .stroke(Color.white, lineWidth: 1)
			 )

	}
}

struct BigStrokedButton_Previews: PreviewProvider {
    static var previews: some View {
        BigStrokedButton(title: "sdf")
    }
}
