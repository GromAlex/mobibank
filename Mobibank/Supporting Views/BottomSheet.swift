//
//  BottomSheet.swift
//  Zay
//
//  Created by Grom on 3/11/20.
//  Copyright © 2020 Peter Williams. All rights reserved.
//

import SwiftUI

enum BottomSheetError: Error {
    case noViewFound(reason: String)
}

struct BottomSheet<SheetContent: View>: ViewModifier {
	@Binding var isPresented: Bool
	let sheetContent: () throws -> SheetContent
	
	@GestureState var translation: CGFloat = 0
	let snapDistance: CGFloat = 10
	
	func body(content: Content) -> some View {
		ZStack(alignment: .bottom) {

			content
			
			if isPresented{
			Color.black.animation(.default).opacity(0.3).edgesIgnoringSafeArea(.top).animation(.default).onTapGesture {
					withAnimation{
						self.isPresented.toggle()
					}
				}
			}
			
			if isPresented {
				
                VStack {
                    Spacer()
                    try? self.sheetContent()
                }

				//.keyboardResponsive()
				.zIndex(.infinity)
				.animation(.default)
				.transition(.move(edge: .bottom))
				.offset(y: self.translation)
				.gesture(
					DragGesture().updating(self.$translation) { value, state, _ in
						//state = max(value.translation.height, 0)
					}.onEnded { value in
						guard abs(value.translation.height) > self.snapDistance else {
							return
						}
						withAnimation{
							self.isPresented = value.translation.height < 0
						}
				})
			}
		}
	}
}

extension View {
	func customBottomSheet<SheetContent: View>(isPresented: Binding<Bool>, sheetContent: @escaping () throws -> SheetContent
	) -> some View {
		self.modifier(BottomSheet(isPresented: isPresented, sheetContent: sheetContent))
	}
}
