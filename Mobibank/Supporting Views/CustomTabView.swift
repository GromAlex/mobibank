//
//  CustomTabView.swift
//  Mobibank
//
//  Created by Grom on 5/11/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import SwiftUI

struct CustomTabView: View {
	var tabs: [String]
	
	@Binding var selectedIndex: Int
	
	let selectedColor = Color.brightAqua
	let unselectedColor = Color.white
	
	var body: some View {
			HStack{
				ForEach(tabs.enumeratedArray(), id: \.element){index, tab in
					Button(action:{
						self.selectedIndex = index
					}){
						VStack(spacing: 9){
							Text(tab.uppercased())
								.font(.custom(self.selectedIndex == index ? Fonts.montserratExtraBold : Fonts.montserratMedium, size: 8))
								.kerning(1.3)
								.foregroundColor(self.selectedIndex == index ? self.selectedColor : self.unselectedColor)
							
							Rectangle().frame(height: 2)
								.foregroundColor(Color.brightAqua)
								.opacity(self.selectedIndex == index ? 1 : 0)
						}
					}
				}
			}
			//.background(Color.lightNavyThree)
	}
}

struct CustomTabView_Previews: PreviewProvider {
    static var previews: some View {
		CustomTabView(tabs: [], selectedIndex: .constant(0))
    }
}
