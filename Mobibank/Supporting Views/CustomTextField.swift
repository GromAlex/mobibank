//
//  CustomTextField.swift
//  Zay
//
//  Created by Grom on 10/15/19.
//  Copyright © 2019 Peter Williams. All rights reserved.
//

import Foundation
import SwiftUI

struct CustomTextField: UIViewRepresentable {

    class Coordinator: NSObject, UITextFieldDelegate {

        @Binding var text: String
        var didBecomeFirstResponder = false

		init(text: Binding<String>) {
            _text = text
        }

        func textFieldDidChangeSelection(_ textField: UITextField) {
            text = textField.text ?? ""
        }
		
		func textFieldShouldReturn(_ textField: UITextField) -> Bool {
			textField.resignFirstResponder()
			return true
		}
    }

    @Binding var text: String
	var placeholder: String?
	var keyboardType: UIKeyboardType = .default
    var isFirstResponder: Bool = false

    func makeUIView(context: UIViewRepresentableContext<CustomTextField>) -> UITextField {
        let textField = UITextField(frame: .zero)
		textField.returnKeyType = .done
		textField.keyboardType = keyboardType
        textField.delegate = context.coordinator

        return textField
    }

    func makeCoordinator() -> CustomTextField.Coordinator {
        return Coordinator(text: $text)
    }

    func updateUIView(_ uiView: UITextField, context: UIViewRepresentableContext<CustomTextField>) {
        uiView.text = text
		uiView.placeholder = placeholder
        if isFirstResponder && !context.coordinator.didBecomeFirstResponder && uiView.window != nil  {
            uiView.becomeFirstResponder()
            context.coordinator.didBecomeFirstResponder = true
        }
    }
}
