//
//  LabelTextField.swift
//  Mobibank
//
//  Created by Grom on 5/14/20.
//  Copyright © 2020 BambooAgile. All rights reserved.
//

import SwiftUI

struct LabelTextField : View {
	var placeHolder: String
	var secure: Bool = false
	@Binding var text: String
	var keyboardType: UIKeyboardType = .default
	
	var body: some View {
		
		ZStack(alignment: .leading) {
			Group{
				if text.isEmpty {
					placeholder
				}
				if secure{
					SecureField("", text: $text)
						.keyboardType(keyboardType)
				}
				else{
					TextField("", text: $text)
						.font(.custom(Fonts.montserratSemibold, size: 15))
						.keyboardType(keyboardType)
						.foregroundColor(.white)
				}
			}
			.padding(.leading, 18)
		}
		.frame(height: 50)
		.background(Color.lightNavyTwo)
		.cornerRadius(5)
	}
	
	var placeholder: some View{
		Text(placeHolder.uppercased())
			.font(.custom(Fonts.montserratSemibold, size: 12))
			.foregroundColor(Color.white.opacity(0.4))
	}
}
