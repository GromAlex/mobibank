//
//  RadioGroup.swift
//  Zay
//
//  Created by Grom on 12/2/19.
//  Copyright © 2019 Peter Williams. All rights reserved.
//

import SwiftUI

struct RadioGroup: View{
	
	@Binding var selectedIndex: Int
	let titles: [String]
	
	var body: some View {
		VStack(alignment: .leading){
			ForEach(0..<titles.count, id: \.self) {index in
				RadioButton(title: self.titles[index], selected: index == self.selectedIndex).onTapGesture {
					self.selectedIndex = index
				}.animation(nil)
			}
		}
	}
}

struct RadioButton: View{
	let title: String
	let selected: Bool

	var body: some View {
		HStack(spacing: 14){
			Group{
				if selected{
					Image(Assets.Icons.defaultActive)
				}
				else{
					Image(Assets.Icons.defaultDisabled)
				}
			}
			Text(title.uppercased()).modifier(ButtonFont(selected: selected)).foregroundColor(.white)
			Spacer()
		}
	}
	
	struct ButtonFont: ViewModifier {
		let selected: Bool
		func body(content: Content)-> some View {
			return content.font(.custom(Fonts.montserratSemibold, size: 12))
		}
	}
}



struct RadioGroupView<Page>: View where Page: View{
	
	@Binding var selectedIndex: Int
	let contents: [Page]
	
	var body: some View {
		VStack(alignment: .leading, spacing: 8){
			ForEach(0..<contents.count) {index in
				RadioButton(content: self.contents[index], selected: index == self.selectedIndex).onTapGesture {
					self.selectedIndex = index
				}.animation(nil)
			}
		}
	}
	
	struct RadioButton<Page>: View where Page: View{
		let content: Page
		let selected: Bool

		var body: some View {
			HStack(alignment: .top, spacing: 14){
				Group{
					if selected{
						Image(Assets.Icons.defaultActive)
					}
					else{
						Image(Assets.Icons.defaultDisabled)
					}
				}.offset(y: 3)
				content
				Spacer()
			}
		}
		
		struct ButtonFont: ViewModifier {
			let selected: Bool
			func body(content: Content)-> some View {
				return content.font(.custom(selected ? "FuturaBT-Book" : "FuturaBT-Light", size: 16))
			}
		}
	}
}


