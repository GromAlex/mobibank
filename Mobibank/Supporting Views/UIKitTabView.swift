//
//  UIKitTabView.swift
//  Sihaty
//
//  Created by Darya Kuliashova on 10.03.2020.
//  Copyright © 2020 Bamboo Agile. All rights reserved.
//

import SwiftUI

struct UIKitTabView: View {
    var viewControllers: [UIHostingController<AnyView>]
	var tabs:[Tab]
	var selectedIndex: Binding<Int>
	
	init(_ tabs: [Tab], selectedIndex: Binding<Int>) {
		self.tabs = tabs
		self.selectedIndex = selectedIndex
        self.viewControllers = tabs.map {
            let host = UIHostingController(rootView: $0.view)
            host.tabBarItem = $0.barItem
            return host
        }
    }

    var body: some View {
        TabBarController(controllers: viewControllers, tabs: tabs, selectedIndex: selectedIndex)
            .edgesIgnoringSafeArea(.all)
    }

    struct Tab {
        var view: AnyView
        var barItem: UITabBarItem

        init<V: View>(view: V, barItem: UITabBarItem) {
            self.view = AnyView(view)
            self.barItem = barItem
        }
    }
}

struct TabBarController: UIViewControllerRepresentable {
    var controllers: [UIViewController]
	var tabs:[UIKitTabView.Tab]
	@Binding var selectedIndex: Int
    
    func updateUIViewController(_ uiViewController: UITabBarController, context: UIViewControllerRepresentableContext<TabBarController>) {
		uiViewController.selectedIndex = selectedIndex
    }

    func makeUIViewController(context: Context) -> UITabBarController {
        let tabBarController = UITabBarController()
        tabBarController.viewControllers = controllers
		tabBarController.delegate = context.coordinator
		tabBarController.selectedIndex = 0
        return tabBarController
    }
	
	func makeCoordinator() -> Coordinator {
		Coordinator(self)
	}
	
	class Coordinator: NSObject, UITabBarControllerDelegate, UITabBarDelegate {
		var parent: TabBarController
		init(_ tabBarController: TabBarController) {
			self.parent = tabBarController
		}
		

		func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
	
			if  parent.selectedIndex == tabBarController.selectedIndex{
				let host = UIHostingController(rootView: parent.tabs[tabBarController.selectedIndex].view)
				host.tabBarItem = parent.tabs[tabBarController.selectedIndex].barItem
				tabBarController.viewControllers![tabBarController.selectedIndex] = host
			}
			
			parent.selectedIndex = tabBarController.selectedIndex
		}
	}
}
